from django.apps import AppConfig


class MessagesConfig(AppConfig):
    name = 'messages'
    label = 'td_messages'
