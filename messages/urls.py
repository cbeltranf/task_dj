from django.urls import re_path, path
from .views import (
    inbox,
    sent_mail,
    MessageView,
    MessageNew,
)

urlpatterns = [
    path('inbox', inbox, name='inbox'),
    path('sent-mail', sent_mail, name='sent_mail'),
    re_path(
        r'message-view/(?P<pk>\d+)/$',
        MessageView.as_view(),
        name='message-view'
    ),
    path('message-new/', MessageNew.as_view(),name='message-new'),
]