from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic.edit import UpdateView, CreateView
from django.urls import reverse_lazy
from django.template.loader import get_template
from django.http import HttpResponse, HttpResponseRedirect
import task
import os

from .models import Message, MessageAttach
from app.models import TaskAssignment
from user_profile.models import User
from django import forms


@login_required(login_url='/login/')
def inbox(request):
    """ profile for user """
    template = get_template('dashboard/inbox.html')
    messages = Message.objects.filter(to_user=request.user.id).order_by("-date")
    messages_inbox = Message.objects.filter(
        to_user=request.user.id,
        read=False
    ).count()
    context = {
     'messages': messages,
     'messages_inbox': messages_inbox,
    }
    return HttpResponse(template.render(context, request))


@login_required(login_url='/login/')
def sent_mail(request):
    """ profile for user """
    template = get_template('dashboard/inbox.html')
    messages = Message.objects.filter(from_user=request.user.id).order_by("-date")
    messages_inbox = Message.objects.filter(to_user=request.user.id,read=False).count()
    context = {
     'active': 'sent',
     'messages': messages,
     'messages_inbox': messages_inbox,
    }
    return HttpResponse(template.render(context, request))


@method_decorator(login_required, name='dispatch')
class MessageView(CreateView):
    """ render update view document or photo task """
    model = Message
    fields = '__all__'
    template_name = "dashboard/message-view.html"

    def get_success_url(self):
        return reverse_lazy('inbox')

    def get_context_data(self, **kwargs):
        context = super(MessageView, self).get_context_data(**kwargs)
        context['message'] = Message.objects.get(pk=self.kwargs['pk'],)
        context['messages_inbox'] = Message.objects.filter(
            to_user=context['message'].
            id,
            read=False
        ).count()
        context['message'].read = True
        context['message'].save()
        return context

    def get(self, request, **kwargs):
        message_id = kwargs['pk']
        Message.objects.filter(pk=message_id).update(read=True)
        return super(MessageView, self).get(request, **kwargs)


def upload_attached(f, filename):
    with open(filename, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)


@method_decorator(login_required, name='dispatch')
class MessageNew(CreateView):
    """ render update view document or photo task """
    model = Message
    fields = '__all__'
    template_name = "dashboard/message-new.html"

    def get_success_url(self):
        return reverse_lazy('sent_mail')

    def get_context_data(self, **kwargs):
        context = super(MessageNew, self).get_context_data(**kwargs)
        context['subject'] = self.request.GET.get('subject', '')
        return context
    
    def get_initial(self, **kwargs):
        initial = {}
        ta = self.request.GET.get('ta', '')
        to = self.request.GET.get('to', '')
        initial['subject'] = self.request.GET.get('subject', '')
        initial['task_assignment'] = ta
        if (ta):
            task_assignment = TaskAssignment.objects.get(pk=ta)
            initial['to_user'] = task_assignment.store.auditor.id
        if (to):
            initial['to_user'] = User.objects.get(pk=to)
        return initial

    def form_valid(self, form):
        self.object = form.save()

        for file in self.request.FILES.getlist('attach'):
            MessageAttach.objects.create(
                message=self.object,
                name=file
            )
        return HttpResponseRedirect(self.get_success_url())
    
    def post(self, request, **kwargs):
        request.POST = request.POST.copy()
        request.POST["type"] = "Message"
        request.POST["from_user"] = request.user.id
        to = request.GET.get('reponse_to', '')
        if to:
            elem = Message.objects.get(pk=to)
            request.POST["reply_to"] = elem.id
        return super(MessageNew, self).post(request, **kwargs)
