# Generated by Django 2.0.7 on 2018-09-26 20:36

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('subject', models.CharField(max_length=100)),
                ('text', models.TextField(blank=True)),
                ('type', models.CharField(choices=[('Message', 'Message'), ('Request', 'Request')], max_length=30)),
                ('date', models.DateTimeField(auto_now_add=True, null=True)),
                ('read', models.BooleanField(default=False)),
            ],
            options={
                'db_table': 'message',
            },
        ),
        migrations.CreateModel(
            name='MessageAttach',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.ImageField(blank=True, upload_to='static/files/messages/')),
                ('message', models.ForeignKey(on_delete='cascade', related_name='attachments', to='td_messages.Message')),
            ],
        ),
    ]
