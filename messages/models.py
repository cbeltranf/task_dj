from django.db import models
from task.middlewares import RequestMiddleware
from datetime import datetime


MESSAGE_TYPE_CHOICES = (
    ('Message', 'Message'),
    ('Request', 'Request'),
)

class MessageAttach(models.Model):
    name = models.ImageField(blank=True, upload_to='static/files/messages/')
    message = models.ForeignKey('Message',  on_delete="cascade", related_name="attachments")


class Message(models.Model):
    """ Model Messages :Message and Request """
    subject         = models.CharField(max_length=100, blank=False, null=False, verbose_name="Asunto",)
    text            = models.TextField(blank=True, verbose_name="Mensaje",)
    type            = models.CharField(max_length=30, choices = MESSAGE_TYPE_CHOICES)
    to_user         = models.ForeignKey('user_profile.User', on_delete=models.CASCADE,null=True, related_name='me_to', verbose_name="Enviar a",)
    from_user       = models.ForeignKey('user_profile.User', on_delete=models.CASCADE,null=True, related_name='me_from',)
    date            = models.DateTimeField(blank=True, null=True, auto_now_add=True)
    read            = models.BooleanField(default=False)
    task_assignment = models.ForeignKey('app.TaskAssignment', on_delete=models.CASCADE,null=True, blank=True, related_name='me_from',verbose_name="Referente a tarea",)
    reply_to = models.ForeignKey('self', on_delete=models.SET_NULL, null=True, blank=True)

    class Meta:
        db_table = 'message'

    def get_date(self):
        today = datetime.now()
        if today.day==self.date.day:
            return "%s:%s" % (self.date.hour, self.date.minute)
        else:
            return self.date

    def is_own(self):
        request = RequestMiddleware(get_response=None)
        request = request.thread_local.current_request
        return self.to_user == request.user

    def __str__(self):
        return "%s" % (self.subject)
