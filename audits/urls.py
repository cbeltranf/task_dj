from django.urls import path
from .views import (
    AuditResults,
    EditAuditForm,
    CreateAuditForm,
    CreateAudit,
    RunVisit,
    AuditReports,
    UpdateVisit
)

urlpatterns = [
    path('audits', AuditResults.as_view(), name='audits'),
    path('create-audit', CreateAudit.as_view(), name='create-audit'),
    path('auditor-reports', AuditReports.as_view(), name='auditor-reports'),
    path('edit-audit-form/<int:pk>', EditAuditForm.as_view(), name='edit-audit-form'),
    path('run-audit/<int:pk>', RunVisit.as_view(), name='run-visit'),
    path('edit-audit/<int:pk>', UpdateVisit.as_view(), name='edit-visit'),
    path(
        'create-audit-form',
        CreateAuditForm.as_view(),
        name='create-audit-form'
    ),
]
