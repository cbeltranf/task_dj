from django.views.generic import ListView
import json
from django.http import JsonResponse
from django.core import serializers
import dateutil.parser
from django.views.generic.edit import UpdateView, CreateView
from django.views.generic import TemplateView
from django.urls import reverse_lazy
from django.views.generic import DetailView
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.db import connection

from .models import (
    AuditProgrammedStoreVisit,
    AuditForm,
    AuditStoreVisit,
    AuditFormField,
    AuditStoreVisitFieldResults,
    AuditStoreVisitFiles,
)
import simplejson as json
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect
from django.forms.models import model_to_dict
import datetime
import time
import sys

class AuditResults(ListView):
    model = AuditProgrammedStoreVisit
    template_name = "auditor/audit_list.html"

    def get_query_set(self):
        return AuditProgrammedStoreVisit.objects().filter(
            store_auditor=self.request.user,
        )


class EditAuditForm(UpdateView):
    model = AuditForm
    fields = '__all__'
    template_name = "auditor/edit-audit-form.html"

    def get_success_url(self):
        return reverse_lazy('audits')

    def get_context_data(self, **kwargs):
        context = (
            super(EditAuditForm, self)
            .get_context_data(**kwargs)
        )
        context['update'] = True
        context['form_fields'] = json.dumps(list(self.object.form_fields.all().values()))
        return context

    def form_valid(self, form):
        request = self.request
        self.object = form.save()
     
        fields = json.loads(request.POST.get('fields', '[]'))
        #raise Exception(json.dumps(list(request.POST)))
        for field in fields:
            if ('id' not in field):
                AuditFormField.objects.create(
                    label=field['label'],
                    audit_form=self.object
                )
        return HttpResponseRedirect(self.get_success_url())


class CreateAuditForm(CreateView):
    model = AuditForm
    fields = '__all__'
    template_name = "auditor/edit-audit-form.html"

    def get_success_url(self):
        return reverse_lazy('audits')

    def get_context_data(self, **kwargs):
        context = (
            super(CreateAuditForm, self)
            .get_context_data(**kwargs)
        )
        return context

    def form_valid(self, form):
        request = self.request
        self.object = form.save()      
        fields = json.loads(request.POST.get('fields', '[]'))
        for field in fields:
            if ('id' not in field):
                AuditFormField.objects.create(
                    label=field['label'],
                    audit_form=self.object
                )
        return HttpResponseRedirect(self.get_success_url())


class CreateAudit(CreateView):
    model = AuditProgrammedStoreVisit
    template_name = "auditor/create-audit.html"
    fields = "__all__"

    def get_success_url(self):
        return reverse_lazy('audits')

    def post(self, request, **kwargs):
        request.POST = request.POST.copy()
        request.POST['date_time_programmed'] = dateutil.parser.parse(
            request.POST['date_time_programmed']
        )
        request.POST['audit_form'] = 1

        return (
            super(CreateAudit, self)
            .post(request, **kwargs)
        )


class RunVisit(DetailView):
    model = AuditProgrammedStoreVisit
    fields = '__all__'
    template_name = "auditor/run-audit.html"

    def post(self, request, **kwargs):
        request.POST = request.POST.copy()
        visit = AuditProgrammedStoreVisit.objects.get(pk=kwargs['pk']) 
        storeVisit=AuditStoreVisit.objects.create(
            action="Create",
            date_time_visit=datetime.datetime.now(),
            location_lat=request.POST['lat'],
            location_lon=request.POST['lon'],
            visit=visit,
            comments=request.POST['comments']
        )
        #new_visit_id = connection.insert_id()
        for field in visit.audit_form.form_fields.all():
            checked = request.POST.get(str(field.id), False) == "on"
            AuditStoreVisitFieldResults.objects.create(
                visit=visit,
                field=field,
                checked=checked
            )

        imcant=range(int(request.POST['cantImg'])+1)

        for cnt in imcant:
            if ('img'+str(cnt) in request.FILES.keys()):
                if request.FILES['img'+str(cnt)]:
                    ts = time.time()
                    myfile = request.FILES['img'+str(cnt)]
                    AuditStoreVisitFiles.objects.create(
                        visit=storeVisit,
                        image=myfile)

        return HttpResponseRedirect(reverse_lazy('audits'))


class UpdateVisit(UpdateView):
    model = AuditProgrammedStoreVisit
    template_name = "auditor/run-audit.html"
    fields = "__all__"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        myVisit = dict(self.object.audit_record_visit.values('id', 'visit_id', "comments").order_by('-id').first())      #desc
        context["initial_data"] = myVisit
        context["initial_files"] = json.dumps(list(AuditStoreVisitFiles.objects.values('image', 'id').filter(visit=myVisit['id'])))
        context["initial_fields"] = json.dumps(list(self.object.audit_result.values('field_id', 'checked')))
        context["my_fields"] = list(self.object.audit_result.values('field_id', 'checked'))
        context["my_files"] = AuditStoreVisitFiles.objects.values('image', 'id').filter(visit=myVisit['id'])
        return context
    

    def post(self, request, **kwargs):
        request.POST = request.POST.copy()
        visit = AuditProgrammedStoreVisit.objects.get(pk=kwargs['pk']) 
        storeVisit=AuditStoreVisit.objects.create(
            action="Update",
            date_time_visit=datetime.datetime.now(),
            location_lat=request.POST['lat'],
            location_lon=request.POST['lon'],
            visit=visit,
            comments=request.POST['comments']
        )
        AuditStoreVisitFieldResults.objects.filter(visit_id=visit).delete()
        for field in visit.audit_form.form_fields.all():
            checked = request.POST.get(str(field.id), False) == "on"
            AuditStoreVisitFieldResults.objects.create(
                visit=visit,
                field=field,
                checked=checked
            )
            
        imcant=range(int(request.POST['cantImg'])+1)
        old_images=[]
        
        for cnt in imcant:
            if request.POST.get('idImage'+str(cnt)):
                old_images.append(request.POST.get('idImage'+str(cnt)))

        #AuditStoreVisitFiles.objects.filter(visit_id=request.POST.get('visit_aud_id')).exclude(pk__in=old_images).delete()
        AuditStoreVisitFiles.objects.filter(pk__in=old_images).update(visit=storeVisit)
       
        #raise Exception(data, request.POST.get('visit_aud_id'), old_images)
        for cnt in imcant:
            if ('img'+str(cnt) in request.FILES.keys()):
                if request.FILES['img'+str(cnt)]:
                    ts = time.time()
                    myfile = request.FILES['img'+str(cnt)]
                    AuditStoreVisitFiles.objects.create(
                        visit=storeVisit,
                        image=myfile)
                    
        return HttpResponseRedirect(reverse_lazy('audits'))


class AuditReports(TemplateView):
    template_name = "auditor/audit-reports.html"