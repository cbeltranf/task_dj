from django.db import models


class AuditForm(models.Model):
    """ audit form """
    last_update = models.DateTimeField(auto_now_add=True)


class AuditFormField(models.Model):
    """ audit form fields """
    audit_form = models.ForeignKey(
        AuditForm,
        on_delete=models.CASCADE,
        related_name="form_fields"
    )
    label = models.CharField(max_length=200, help_text="Elemento a evaluar")


class AuditProgrammedStoreVisit(models.Model):
    """ Programmed store visits """
    date_time_programmed = models.DateTimeField(help_text="Fecha y hora de visita:", verbose_name="Fecha y hora de visita")
    store = models.ForeignKey(
        'store.store',
        on_delete=models.CASCADE,
        verbose_name="Tienda:"
    )
    audit_form = models.ForeignKey(
        'AuditForm',
        on_delete=models.CASCADE,
        help_text="Formulario de auditoria"
    )

    def calculate_audit_result(self):
        responses = self.audit_result.all()
        total_responses = responses.count()
        if total_responses == 0:
            return 0
        else:
            total_cheched = 0
            for res in responses:
                if (res.checked):
                    total_cheched += 1
            return int((total_cheched * 100) / total_responses)



class AuditStoreVisit(models.Model):
    """ Record from visits (creation or update) """
    visit = models.ForeignKey(
        AuditProgrammedStoreVisit,
        related_name="audit_record_visit",
        on_delete=models.CASCADE
    )
    action = models.CharField(max_length=30, help_text="Tipo de acción llevada acabo (Create or Update)")
    date_time_visit = models.DateTimeField(help_text="Fecha y hora de visita:", verbose_name="Fecha y hora de visita")
    location_lat = models.CharField(max_length=50, help_text="Location Latitude")
    location_lon = models.CharField(max_length=50, help_text="Location Longitude")
    comments = models.TextField(help_text="Comentarios generales de la visita", verbose_name='Comentarios')


class AuditStoreVisitFiles(models.Model):
    """ Record from visits (creation or update) """
    visit = models.ForeignKey(
        AuditStoreVisit,
        related_name="audit_store_visit",
        on_delete=models.CASCADE
    )
    image = models.ImageField(blank=True, upload_to='static/files/visit_evidence/')

class AuditStoreVisitFieldResults(models.Model):
    """ Result of audits """
    visit = models.ForeignKey(
        AuditProgrammedStoreVisit,
        related_name="audit_result",
        on_delete=models.CASCADE
    )
    field = models.ForeignKey(
        'AuditFormField',
        on_delete=models.CASCADE
    )
    checked = models.BooleanField()
