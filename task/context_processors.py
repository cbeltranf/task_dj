import os 

def export_vars(request):
    data = {}
    data['METABASE_SERVER'] = os.environ.get('METABASE_SERVER')
    return data