"""
task URL Configuration
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import re_path, include
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth import views as auth_views
from app import views
from rest_framework import routers

# routers API REST
router = routers.DefaultRouter()
router.register(r'task-form-field', views.TaskFormFieldsViewSet)


urlpatterns = [
    re_path(r'^', include('user_profile.urls')),
    re_path(r'^', include('messages.urls')),
    re_path(r'^', include('store.urls')),
    re_path(r'^', include('project.urls')),
    re_path(r'^', include('audits.urls')),
    # Administrador
    path('administrator/', admin.site.urls),
    path('administrator/doc/', include('django.contrib.admindocs.urls')),
    # Dashboard
    path('', views.dashboard, name='dashboard'),
    # Auditor
    re_path(
        r'update-rating-task/(?P<pk>\d+)/$',
        views.UpdateRatingTaskView.as_view(),
        name='update-rating-task',
    ),
    re_path(
        r'new-rate-task/(?P<pk>\d+)/$',
        views.CreateRatingTaskView.as_view(),
        name='create-rating-task',
    ),
    # Operator
    path(
        'calendar-operator',
        views.calendar_operator,
        name='calendar-operator'
    ),
    re_path(
        r'task-execute-new-document/(?P<pk>\d+)/$',
        views.TaskExecuteDocOperatorNew.as_view(),
        name='task-execute-doc-new'
    ),
    re_path(
        r'task-execute-update-document/(?P<pk>\d+)/$',
        views.TaskExecuteDocOperatorUpdate.as_view(),
        name='task-execute-doc-update'
    ),
    re_path(
        r'task-administrator-remove/(?P<pk>\d+)/$',
        views.TaskDelete.as_view(),
        name='task-delete'
    ),
    re_path(
        r'task-execute-new-form/(?P<pk>\d+)/$',
        views.TaskExecuteFormOperatorNew.as_view(),
        name='task-execute-form-new'
    ),
    re_path(
        r'task-execute-update-form/(?P<pk>\d+)/$',
        views.TaskExecuteFormOperatorUpdate.as_view(),
        name='task-execute-form-update'
    ),
    # Administrador
    path(
        'reports-administrator/',
        views.reports_administrator,
        name='reports_administrator'
    ),
    path(
        'help-administrator/',
        views.Help.as_view(),
        name='help-administrator'
    ),
    path(
        'help-operator/',
        views.HelpOperator.as_view(),
        name='help-operator'
    ),
    re_path(
        r'task-administrator/',
        views.task_administrator,
        name='task_administrator'
    ),
    re_path(
        r'assign-task/(?P<pk>\d+)/$',
        views.AssignTask.as_view(),
        name='task_assign'
    ),
    re_path(
        r'task-administrator-update/(?P<pk>\d+)/$',
        views.TaskAdministratorUpdateDoc.as_view(),
        name='task_administrator_update_document'
    ),
    re_path(
        r'task-administrator-update-modal',
        views.TaskAdministratorUpdateDocModal.as_view(),
        name='task_administrator_update_document_modal'
    ),
    re_path(
        r'task-administrator-new-document/',
        views.TaskAdministratorNewDoc.as_view(),
        name='task_administrator_new_document'
    ),
    re_path(r'^task-execute/(?P<store_id>\d+|)/(?P<task_id>\d+|)/(?P<project_id>\d+|)', views.StoreTaskExecute.as_view()),
    path('task-upload-evidence/', views.upload_task_evidence),
    path('task-audit/<int:result_id>', views.TaskAudit.as_view()),
    path('login/', auth_views.login, {'template_name': 'site/login.html'}),
    path('/', auth_views.login, {'template_name': 'site/login.html'}),
    path('forgot-password/', auth_views.password_reset, {'template_name': 'site/forgot_password.html'}, name='password_reset'),
    path('password_reset/done/', auth_views.password_reset_done, {'template_name': 'site/password_reset_done.html'}, name='password_reset_done'),
    path('logout/', views.log_out, name='logout'),
    # API REST
    re_path(r'^', include(router.urls)),
    re_path(r'^api-auth/', include(
        'rest_framework.urls', namespace='rest_framework')),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
