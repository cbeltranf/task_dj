FROM python:3.5
ENV PYTHONUNBUFFERED 1
RUN mkdir /task
WORKDIR /task
ADD requirements.txt /task/
RUN pip install -r requirements.txt
ADD . /task/
RUN apt-get update
RUN apt-get install binutils libproj-dev gdal-bin -y


