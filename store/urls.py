from django.urls import re_path, path, include
from .views import (
    store_administrator,
    StoreAdministratorUpdate,
    StoreAdministratorNew,
    LocationViewset,
)
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'location', LocationViewset)

urlpatterns = [
    re_path(r'store/', include(router.urls)),
    re_path(r'store-administrator/', store_administrator,name='store_administrator'),
    re_path(r'store-administrator-update/(?P<pk>\d+)/$', StoreAdministratorUpdate.as_view(),name='store_administrator_update'),
    re_path(r'store-administrator-new/', StoreAdministratorNew.as_view(),name='store_administrator_new'),
]