from django.template.loader import get_template
from django.http import HttpResponse
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django import forms
from django.contrib.auth.decorators import login_required
from django.views.generic.edit import UpdateView, CreateView
from django.forms.widgets import FileInput
from rest_framework import viewsets
from .models import Store, Location, LOCATION_TYPE_CHOICES
from .serializers import LocationSerializer
from mapwidgets.widgets import GooglePointFieldWidget
from user_profile.models import User

import json
from django.urls import reverse_lazy

@login_required(login_url='/login/')
def store_administrator(request):
    """ profile for user """
    template = get_template('administrator/store-administrator.html')
    store= Store.objects.filter(active=True).order_by('-created_at')
    context = {
    'store': store,
    }
    return HttpResponse(template.render(context, request))


class StoreCreateForm(forms.ModelForm):
    class Meta:
       model = Store
       fields = [
           'name',
           'image',
           'location',
           'addres',
           'active',
           'manager',
           'auditor',
           'phone',
           'zone',
           'email',
           'coords',
        ]

    def __init__(self, *args, **kwargs):
       super(StoreCreateForm, self).__init__(*args, **kwargs)
       self.fields['location'].queryset = Location.objects.filter(
           type="Country"
       )
       self.fields['zone'].queryset = Location.objects.filter(
           type="Zone"
       )
       self.fields['manager'].queryset = User.objects.filter(
           groups__id=3
       )
       self.fields['auditor'].queryset = User.objects.filter(
           groups__id=1
       )


@method_decorator(login_required, name='dispatch')
class StoreAdministratorUpdate(UpdateView):
    """ render update view form """
    form_class = StoreCreateForm
    model = Store
    template_name = "administrator/store-administrator-form.html"

    def get_form(self, form_class=None):
        form = super(StoreAdministratorUpdate, self).get_form(form_class)
        form.fields['image'].widget = FileInput()
        form.fields['coords'].widget = GooglePointFieldWidget()
        return form

    def get_success_url(self):
        return reverse_lazy('store_administrator')
    
    def get_context_data(self, **kwargs):
        context = super(StoreAdministratorUpdate, self).get_context_data(**kwargs)
        context['update'] = True
        context['zones'] = json.dumps(
            list(Location.objects.filter(type="Zone").values('name', 'id'))
        )
        return context


@method_decorator(login_required, name='dispatch')
class StoreAdministratorNew(CreateView):
    """ render update view document or photo task """
    form_class = StoreCreateForm
    template_name = "administrator/store-administrator-form.html"

    def get_form(self, form_class=None):
        form = super(StoreAdministratorNew, self).get_form(form_class)
        form.fields['image'].widget = FileInput()
        form.fields['coords'].widget = GooglePointFieldWidget()
        return form

    def get_success_url(self):
        return reverse_lazy('store_administrator')

    def get_context_data(self):
        context = super(StoreAdministratorNew, self).get_context_data()
        context['zones'] = json.dumps(
            list(Location.objects.filter(type="Zone").values('name', 'id'))
        )
        return context


class LocationViewset(viewsets.ModelViewSet):
    queryset = Location.objects.all()
    serializer_class = LocationSerializer
