from django.contrib.gis.db import models

"""
Atributos choice para los campos
"""
LOCATION_TYPE_CHOICES = (
    ('Country', 'Country'),
    ('Province', 'Province'),
    ('City', 'City'),
    ('Zone', 'Zona'),
)


class Location(models.Model):
    """ Model location (longitude y latitude) """
    name = models.CharField(
        max_length=100,
        null=False,
        blank=False,
        default=None
    )
    type = models.CharField(max_length=20, choices=LOCATION_TYPE_CHOICES)
    parent = models.ForeignKey(
        'self',
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        related_name='lo_location'
    )
    longitude = models.DecimalField(max_digits=8, decimal_places=6, default=0)
    latitude = models.DecimalField(max_digits=8, decimal_places=6, default=0)
    iso = models.CharField(
        max_length=3,
        null=True,
        blank=True,
        default=None,
        verbose_name="ISO"
    )

    class Meta:
        db_table = 'location'

    def __str__(self):
        if self.parent is None:
            return "%s" % (self.name)
        else:
            return "%s (%s)" % (
                self.name,
                self.parent.name
            )


# Create your models here.
class Store(models.Model):
    """ Model store """
    name = models.CharField(
        max_length=50,
        null=False,
        blank=False,
        default=None,
        verbose_name="Nombre"
    )
    image = models.ImageField(
        upload_to='static/files/store/',
        default='static/files/store/no-image.jpg',
        verbose_name="Fotografía",
        blank=True,
    )
    location = models.ForeignKey(
        Location,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        default=None,
        verbose_name="País",
        related_name="country"
    )
    zone = models.ForeignKey(
        Location,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        default=None,
        verbose_name="Zona",
        related_name="zone"
    )
    addres = models.CharField(
        max_length=100,
        null=False,
        default=None,
        verbose_name="Dirección"
    )
    active = models.BooleanField(default=True, verbose_name="Habilitada")
    manager = models.ForeignKey(
        'user_profile.User',
        on_delete=models.DO_NOTHING,
        blank=True,
        null=True,
        related_name='manager',
        verbose_name="Gerente"
    )
    auditor = models.ForeignKey(
        'user_profile.User',
        on_delete=models.DO_NOTHING,
        blank=True,
        null=True,
        related_name='auditor',
        verbose_name="Director Regional"
    )
    phone = models.CharField(
        max_length=16,
        null=True,
        blank=True,
        default=None,
        verbose_name="Teléfono"
    )
    email = models.EmailField(
        verbose_name="Correo de la tienda"
    )
    coords = models.PointField(null=True, blank=True, verbose_name="Coordenadas")
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'store'

    def __str__(self):
        return self.name
    

