# Generated by Django 2.0.7 on 2018-09-26 20:36

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Location',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default=None, max_length=100)),
                ('type', models.CharField(choices=[('Country', 'Country'), ('Province', 'Province'), ('City', 'City')], max_length=20)),
                ('longitude', models.DecimalField(decimal_places=6, default=0, max_digits=8)),
                ('latitude', models.DecimalField(decimal_places=6, default=0, max_digits=8)),
                ('iso', models.CharField(blank=True, default=None, max_length=3, null=True, verbose_name='ISO')),
            ],
            options={
                'db_table': 'location',
            },
        ),
        migrations.CreateModel(
            name='Store',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default=None, max_length=50, verbose_name='Name')),
                ('addres', models.CharField(default=None, max_length=100, verbose_name='Address')),
                ('active', models.BooleanField(default=True)),
                ('phone', models.CharField(blank=True, default=None, max_length=16, null=True)),
            ],
            options={
                'db_table': 'store',
            },
        ),
    ]
