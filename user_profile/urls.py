from django.urls import re_path, path
from .views import (
    ProfileUpdate,
    UserDetailView,
    user_administrator,
    UserDetailAdmin,
    CreateUser,
    UpdateUser,
)

urlpatterns = [
    path(r'profile/', ProfileUpdate.as_view(),name='profile'),
    re_path(r'user-administrator/', user_administrator,name='store_administrator'),
    re_path(r'user-administrator-detail/(?P<pk>\d+)/$', UserDetailAdmin.as_view(), name='user_detail'),
    re_path(r'create-user/', CreateUser.as_view(), name='create_user'),
    path(r'update-user/<int:pk>/', UpdateUser.as_view(), name='update_user'),
    re_path(r'^user/(?P<pk>\d+)/$', UserDetailView.as_view(), name='user'),
]