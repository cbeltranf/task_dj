from django.contrib import admin
from .models import Client, User
# Register your models here.
class ClientAdmin(admin.ModelAdmin):
    pass

class UserAdmin(admin.ModelAdmin):
    pass

admin.site.register(Client, ClientAdmin)
admin.site.register(User, UserAdmin)