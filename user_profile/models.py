from django.db import models
from django.contrib.auth.models import AbstractUser
from messages.models import Message
from django.utils import timezone


CLIENT_THEME_CHOICES = (
    ('skin-default', 'Gray'),
    ('skin-green', 'Green'),
    ('skin-red', 'Red'),
    ('skin-blue', 'Blue'),
    ('skin-purple', 'Purple'),
    ('skin-megna', 'Megna'),
    ('skin-default-dark', 'Gray Dark'),
    ('skin-green-dark', 'Green Dark'),
    ('skin-red-dark', 'Red Dark'),
    ('skin-blue-dark', 'Blue Dark'),
    ('skin-purple-dark', 'Purple Dark'),
    ('skin-megna-dark', 'Megna Dark'),
)

"""
Modelos
""" 
class Client(models.Model):
    """
    Model Client for contact, info and app configuration  
    """
    name = models.CharField(max_length=100,null=False,blank=False,default=None,verbose_name="Name")
    logo = models.ImageField(blank=True, upload_to='static/files/logo/', default='static/files/logo/no-image.png')
    theme  = models.CharField(max_length=20, choices=CLIENT_THEME_CHOICES, default='skin-default')

    class Meta:
        db_table = 'client'

    def __str__(self):
        return self.name


class User(AbstractUser):
    """Custom User model """
    email = models.EmailField('Correo', unique=True)
    mobile = models.CharField(max_length=16,null=True,blank=True,default=None, verbose_name="Teléfono")
    avatar = models.ImageField(blank=True, upload_to='static/files/avatar/', default='static/files/avatar/no-image.png', verbose_name="Foto")
    client = models.ForeignKey(Client, on_delete=models.CASCADE,blank=True,null=True,default=None, verbose_name="Empresa")

    def get_fullname(self):
        """ retun user fullname  """
        return "%s %s" % (self.first_name,self.last_name)

    def get_profile(self):
        """ retun user fullname  """
        p = self.groups.values_list('name',flat=True).first()
        return p

    def get_messages_new(self):
        """ retun user fullname  """
        messages = Message.objects.filter(to_user=self,read=False)
        def count(self):
            return len(messages)
        return messages

    class Meta:
        db_table = 'user'

    def __str__(self):
        return self.get_fullname()

    def __unicode__(self):
        return self.get_fullname()