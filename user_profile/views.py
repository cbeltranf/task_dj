from django.contrib.auth.decorators import login_required
from django.template.loader import get_template
from django.utils.decorators import method_decorator
from django.views.generic.edit import UpdateView, CreateView
from django.views.generic import DetailView, ListView
from django.contrib.auth.hashers import make_password
from .models import User
from django.http import HttpResponse
from django.urls import reverse_lazy
from django import forms
from django.forms.widgets import FileInput


@method_decorator(login_required, name='dispatch')
class ProfileUpdate(UpdateView):
    """ profile for user """
    model = User 
    fields = ('first_name','last_name','avatar','mobile','email',)
    template_name = "dashboard/profile.html"
  

    def get_form(self, form_class=None):
        form = super(ProfileUpdate, self).get_form(form_class)
        form.fields['avatar'].widget = FileInput()
        return form

    def get_success_url(self):
        return reverse_lazy('profile')

    def get_object(self):
        return self.request.user


class UserDetailView(DetailView):
    model = User
    fields = '__all___'
    template_name = "auditor/user.html"


@login_required(login_url='/login/')
def user_administrator(request):
    """ profile for user """
    template = get_template('administrator/user-administrator.html')
    user= User.objects.filter(client=request.user.client)
    context = {
    'user': user,
    }
    return HttpResponse(template.render(context, request))


class UserDetailAdmin(DetailView):
    model = User
    fields = '__all___'
    template_name = "administrator/user-administrator-detail.html"


class CreateUser(CreateView):
    model = User
    fields = ('avatar', 'first_name', 'last_name', 'email', 'mobile', 'username', 'password', 'groups',)
    template_name = "user/create_user.html"

    def get_success_url(self):
        return '/user-administrator/'

    def get_form(self, form_class=None):
        form = super(CreateUser, self).get_form(form_class)
        form.fields['password'].widget = forms.PasswordInput()
        return form

    def form_valid(self, form):
        clean = form.cleaned_data
        new_password = clean.get('password')
        if new_password:
            #encrypt plain password
            form.instance.password = make_password(new_password)
        form.instance.client = self.request.user.client
        return super(CreateView, self).form_valid(form)


class UpdateUser(UpdateView):
    model = User
    fields = ('avatar', 'first_name', 'last_name', 'email', 'mobile', 'username', 'password', 'groups')
    template_name = "user/create_user.html"

    def get_success_url(self):
        return '/user-administrator/'

    def get_form(self, form_class=None):
        form = super(UpdateUser, self).get_form(form_class)
        form.fields['password'].widget = forms.PasswordInput()
        form.fields['avatar'].widget = FileInput()
        return form

    def form_valid(self, form):
        clean = form.cleaned_data
        new_password = clean.get('password')
        if new_password:
            #encrypt plain password
            form.instance.password = make_password(new_password)
        return super(UpdateUser, self).form_valid(form)