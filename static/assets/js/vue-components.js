function createTaskRegisterApp(onCreateCallback) {
    new Vue({
        el: '#vue-app',
        delimiters: ['[[', ']]'],
        mounted: function() {
            var elems = Array.prototype.slice.call(
                document.querySelectorAll('.js-switch')
            );
            elems.forEach(function(el) {
                var switchery = new Switchery(el);
            });
        },
        data: {
            temp_option: '',
            arrayOptions: [],
            errors: {},
            multipleOptions: [],
            addedQuestions: addedQuestions ? addedQuestions : [],
            label: '',
            options: '',
            start_hour: start_hour !== 'None' ? start_hour : '00:00',
            end_hour: end_hour !== 'None' ? end_hour : '23:59',
            type: task_type !== 'None' ? task_type : 'document',
            input_type: 'open-ended-question', 
        },
        computed: {
            isMultiChoice: function () {
                return ['multiple-choice'].indexOf(this.input_type) !== -1;
            }
        },
        mounted: function() {
            setTimeout(function() {
                var elems = Array.prototype.slice.call(
                    document.querySelectorAll('.js-switch-1')
                );
                elems.forEach(function(el) {
                    var switchery = new Switchery(el);
                });
            }, 100)
        },
        methods: {
            validate: function() {
                const errors = {};
                if (!this.input_type) {
                    errors.input_type = 'Seleccione uno';
                }
                if (!this.label) {
                    errors.label = 'Campo requerido';
                }
                if (this.isMultiChoice && !this.options) {
                    errors.options = 'Campo requerido';
                }
                this.errors = errors;
            },
            submit: function() {
                var t = this;
                t.validate();
                if (Object.keys(t.errors).length === 0) {
                    var data = {
                        input_type: t.input_type,
                        label: t.label,
                        options: t.options,
                        optional: t.optional,
                    };
                    t.addedQuestions.push(data);
                    t.input_type = 'open-ended-question';
                    t.label = '';
                    t.options = '';
                    t.arrayOptions = [];
                    t.temp_option = '';
                }
            },
            getChoices: function(question) {
                return question.options.split(',');
            },
            onChangeType: function() {},
            removeQuestion: function(index) {
                if ('undefined' !== typeof this.addedQuestions[index].id) {
                    var id = this.addedQuestions[index].id;
                    $.ajax({
                        url: '/task-form-field/' + id + '/',
                        method: 'delete',
                    });
                }
                this.addedQuestions.splice(index, 1);
            },
            addOption: function () {
                var t = this;
                t.validate();
                if (this.temp_option) {
                    this.arrayOptions = [...this.arrayOptions, this.temp_option]
                    this.temp_option = '';
                    t.options = this.arrayOptions.join(',');
                }
            },
            createTask: function(ev) {
                onCreateCallback(ev);
            }
        },
    });
}