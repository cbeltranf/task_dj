from django.urls import reverse_lazy
from django.template.loader import get_template
from django.contrib.auth.decorators import login_required
from django.views.generic.edit import UpdateView, CreateView, DeleteView
from django.utils.decorators import method_decorator
from django.http import HttpResponse, HttpResponseRedirect
from django.forms.widgets import FileInput
import json
from .models import Project
from app.models import Task, TaskAssignment
from store.models import Store, Location


@login_required(login_url='/login/')
def project_administrator(request):
    """ profile for user """
    template = get_template('administrator/project-administrator.html')
    project = Project.objects.order_by('-created_at')
    context = {
        'project': project,
    }
    return HttpResponse(template.render(context, request))


@method_decorator(login_required, name='dispatch')
class ProjectAdministratorUpdate(UpdateView):
    """ render update view form """
    model = Project
    fields = '__all__'
    template_name = "administrator/project-administrator-form.html"

    def get_success_url(self):
        return reverse_lazy('project_administrator')

    def get_context_data(self, **kwargs):
        context = (
            super(ProjectAdministratorUpdate, self)
            .get_context_data(**kwargs)
        )
        context['update'] = True
        context['selected_tasks'] = json.dumps(
            list(map(lambda ta: { 'id': ta.task.id, 'name': ta.task.name }, self.object.task_assignments.all()))
        )
        context['unselected_tasks'] = json.dumps(
            list(Task.objects.exclude(id__in=self.object.task_assignments.values("task_id")).values("id", "name"))
        )
       # json.dumps(list(
       #     Task.objects.exclude(id__in=self.object.task_assignments.values("id")).values('id', 'name'))
       # )
        context['stores'] = Store.objects.filter(manager__client=self.request.user.client)
        context['countries'] = Location.objects.filter(type="Country")
        context['zones'] = Location.objects.filter(type="Zone").values('name', 'id')
        context['selected_stores'] = list(map(lambda x: x['id'], self.object.stores.values('id')))
        return context

    def get_form(self, form_class=None):
        form = super(ProjectAdministratorUpdate, self).get_form(form_class)
        form.fields['image'].widget = FileInput()
        return form

    def form_valid(self, form):
        self.object = form.save()
        request = self.request
        selected_tasks = request.POST.getlist("selected_tasks")
        stores = request.POST.getlist("stores")

        TaskAssignment.objects.filter(project=self.object).delete()
        
        for task_id in selected_tasks:
            t = Task.objects.get(pk=task_id)
            #t.project.add(self.object)
            TaskAssignment.objects.create(
               task=t,
               project=self.object
            )

        tasks = self.object.task_assignments.exclude(
            task_id__in=selected_tasks
        )

        for t in tasks:
            t.project.remove(self.object)
        
        self.object.stores.set(
            Store.objects.filter(id__in=stores)
        )

        return HttpResponseRedirect(self.get_success_url())


@method_decorator(login_required, name='dispatch')
class ProjectAdministratorNew(CreateView):
    """ render update view document or photo task """
    model = Project
    fields = '__all__'
    template_name = "administrator/project-administrator-form.html"
    def get_success_url(self):
        return reverse_lazy('project_administrator')

    def get_context_data(self, **kwargs):
        context = (
            super(ProjectAdministratorNew, self)
            .get_context_data(**kwargs)
        )
        context['selected_tasks'] = json.dumps([])
        context['unselected_tasks'] = json.dumps(list(Task.objects.values('id', 'name')))
        context['stores'] = Store.objects.filter(
            manager__client=self.request.user.client
        )
        context['countries'] = Location.objects.filter(type="Country")
        context['zones'] = Location.objects.filter(type="Zone").values('name', 'id')
        context['selected_stores'] = []
        return context

    def form_valid(self, form):
        self.object = form.save()
        request = self.request
        for task_id in request.POST.getlist("selected_tasks"):
           t = Task.objects.get(pk=task_id)
           # project = Project.objects.get(pk=self.object.pk)
           # t.project.add(project)
           TaskAssignment.objects.create(
               task=t,
               project=self.object
            )
        return HttpResponseRedirect(self.get_success_url())


class ProjectAdministratorRemove(DeleteView):
    model = Project
    success_url = reverse_lazy('project_administrator')