from django.db import models
from app.models import (
    Task,
)

PROJECT_TYPE_CHOICES = (
    ('Public', 'Public'),
    ('Private', 'Private'),
)


class Project(models.Model):
    """ Model project """
    name = models.CharField(
        max_length=50,
        null=False,
        blank=False,
        default=None,
        verbose_name="Nombre"
    )
    owner = models.ForeignKey('user_profile.User', on_delete=models.CASCADE, blank=True, null=True, )
    active = models.BooleanField(default=True, verbose_name="Proyecto activo")
    type = models.CharField(
        blank=True,
        max_length=20,
        choices=PROJECT_TYPE_CHOICES,
        default='Public',
        verbose_name="Visibilidad del proyecto"
    )
    description = models.TextField(verbose_name='Descripción')
    start_date = models.DateField(verbose_name='Fecha de inicio')
    end_date = models.DateField(verbose_name='Fecha de finalización')
    stores = models.ManyToManyField('store.Store', blank=True)
    image = models.ImageField(
        verbose_name="Imagen",
        upload_to='static/files/projects/',
        default="static/files/projects/no-image.jpg",
        blank=True,
        null=True
    )
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'project'

    def __str__(self):
        return self.name

    def get_active_tasks(self):
        t = Task.objects.filter(id__in=self.task_assignments.filter(project_id=self).values("task_id"), active=True)
       # raise Exception(t) 
        return t
