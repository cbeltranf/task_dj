# Generated by Django 2.0.7 on 2018-10-10 05:45

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0007_auto_20181010_0427'),
        ('app', '0006_auto_20181002_2106'),
        
    ]

    operations = [
        migrations.RunSQL("""CREATE TRIGGER tg_add_relations_task_store_x_project_new_task AFTER INSERT ON task_project
FOR EACH ROW
BEGIN
    DECLARE done INT DEFAULT FALSE;
    DECLARE my_store_id INTEGER;
    DECLARE cur CURSOR FOR SELECT store_id FROM project_stores WHERE project_id = NEW.project_id;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    OPEN cur;
        ins_loop: LOOP
            FETCH cur INTO my_store_id;
            IF done THEN
                LEAVE ins_loop;
            END IF;
            IF (SELECT COUNT(id) FROM task_assignment 
               WHERE store_id=my_store_id AND task_id=NEW.task_id AND project_id=NEW.project_id) < 1
            THEN
            INSERT INTO task_assignment (store_id, task_id, project_id) 
            VALUES (my_store_id, NEW.task_id, NEW.project_id);
            END IF;
        END LOOP;
    CLOSE cur;
 END;""")
    ]
