# Generated by Django 2.0.7 on 2018-10-02 21:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0002_project_owner'),
    ]

    operations = [
        migrations.AlterField(
            model_name='project',
            name='active',
            field=models.BooleanField(default=True, verbose_name='Nombre'),
        ),
        migrations.AlterField(
            model_name='project',
            name='name',
            field=models.CharField(default=None, max_length=50, verbose_name='Nombre'),
        ),
        migrations.AlterField(
            model_name='project',
            name='type',
            field=models.CharField(blank=True, choices=[('Public', 'Public'), ('Private', 'Private')], default='Public', max_length=20, verbose_name='Visibilidad del proyecto'),
        ),
    ]
