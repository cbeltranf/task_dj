# Generated by Django 2.0.7 on 2018-10-10 03:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0010_auto_20181003_0316'),
        ('project', '0004_auto_20181003_0057'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='stores',
            field=models.ManyToManyField(to='store.Store'),
        ),
    ]
