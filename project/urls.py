from django.conf.urls import re_path
from .views import (
    project_administrator,
    ProjectAdministratorUpdate,
    ProjectAdministratorNew,
    ProjectAdministratorRemove,
)

urlpatterns = [
    re_path(
        r'project-administrator/',
        project_administrator,
        name='project_administrator'
    ),
    re_path(
        r'project-administrator-update/(?P<pk>\d+)/$',
        ProjectAdministratorUpdate.as_view(),
        name='project_administrator_update'
    ),
    re_path(
        r'project-administrator-remove/(?P<pk>\d+)/$',
        ProjectAdministratorRemove.as_view(),
        name='project_administrator_remove'
    ),
    re_path(
        r'project-administrator-new/',
        ProjectAdministratorNew.as_view(),
        name='project_administrator_new'
    ),
]
