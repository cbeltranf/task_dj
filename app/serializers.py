from app.models import *
from rest_framework import serializers


class TaskFormFieldsSerializer(serializers.ModelSerializer):
    class Meta:
        model = TaskFormFields
        exclude = ()


class TaskSerializer(serializers.ModelSerializer):
    formFields = TaskFormFieldsSerializer(many=True)
    class Meta:
        model = Task
        exclude = ()



