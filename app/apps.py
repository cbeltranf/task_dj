from django.apps import AppConfig
from django.db.models.signals import post_save
from .signals import asign_task
from .models import Task


class ProjectConfig(AppConfig):
    name = 'app'

    def ready(self):
        post_save.connect(asign_task, sender=Task)
