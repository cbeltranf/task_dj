# Generated by Django 2.0.7 on 2018-10-17 21:41

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0022_auto_20181017_2100'),
    ]

    operations = [
        migrations.AlterField(
            model_name='taskassignmentresult',
            name='project',
            field=models.ForeignKey(null=True, blank=True, on_delete=django.db.models.deletion.CASCADE, related_name='project_task_responses', to='project.Project'),
        ),
    ]
