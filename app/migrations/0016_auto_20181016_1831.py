# Generated by Django 2.0.7 on 2018-10-16 18:31

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0013_auto_20181010_1056'),
        ('store', '0014_auto_20181012_1600'),
        ('app', '0015_remove_taskassignment_project'),
    ]

    operations = [
        migrations.CreateModel(
            name='TaskStore',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('store', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='store.Store')),
            ],
        ),
        migrations.RemoveField(
            model_name='taskassignment',
            name='coords',
        ),
        migrations.RemoveField(
            model_name='taskassignment',
            name='store',
        ),
        migrations.RemoveField(
            model_name='taskformresponse',
            name='task_assign',
        ),
        migrations.AddField(
            model_name='task',
            name='store',
            field=models.ManyToManyField(blank=True, null=True, related_name='tasks', to='store.Store', verbose_name='Tienda'),
        ),
        migrations.AddField(
            model_name='taskassignment',
            name='project',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='task_assignments', to='project.Project'),
        ),
        migrations.AddField(
            model_name='taskformresponse',
            name='assignment_result',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='app.TaskAssignmentResult'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='taskstore',
            name='task',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.Task'),
        ),
        migrations.AddField(
            model_name='taskassignmentresult',
            name='task_store_assignment',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='task_store_assignment', to='app.TaskStore'),
        ),
    ]
