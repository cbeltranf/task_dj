# Generated by Django 2.0.7 on 2018-10-17 16:49

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0018_remove_task_project'),
    ]

    operations = [
          migrations.RunSQL("""CREATE VIEW vw_project_and_store_task_assigments AS
                                    SELECT  t.id AS task_id, 
                                        t.`name` AS task_name, 
                                        ta.id AS assignment_to_project_id,  
                                        p.id AS project_id, 
                                        p.`name` AS project_name, 
                                        ts.id AS assignment_to_store_id,
                                        s.id AS store_id, 
                                        s.name AS store_name  
                                    FROM task AS t
                                    LEFT JOIN `task_assignment` AS ta ON ta.`task_id` = t.`id`
                                    LEFT JOIN `project` AS p ON p.`id` = ta.`project_id`
                                    LEFT JOIN `app_taskstore` AS ts ON ts.task_id = t.id
                                    LEFT JOIN `store` AS s ON s.id = ts.store_id;""")
    ]
