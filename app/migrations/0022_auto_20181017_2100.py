# Generated by Django 2.0.7 on 2018-10-17 21:00

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0021_auto_20181017_2042'),
    ]

    operations = [
        migrations.AlterField(
            model_name='taskassignmentresult',
            name='project',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='project_task_responses', to='project.Project'),
        ),
        migrations.AlterField(
            model_name='taskformresponse',
            name='assignment_result',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='form_responses', to='app.TaskAssignmentResult'),
        ),
    ]
