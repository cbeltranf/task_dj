from django.contrib import admin
from .models import (
    FormField,
    Task,
    TaskAssignment,
    TaskAssignmentResult
)
from messages.models import (
    Message,
)
from store.models import (
    Location,
    Store,
)
from project.models import (Project)


class FormInline(admin.TabularInline):
    model = FormField
    fields = ("type", "label", "min_value", "max_value", "option", "result",)
    extra = 0


class TaskAdmin(admin.ModelAdmin):
    list_display = ('name', 'day_number', 'type', 'periodicity', 'days')
    inlines = [FormInline, ]


class LocationAdmin(admin.ModelAdmin):
    pass


class StoreAdmin(admin.ModelAdmin):
    pass


class ProjectAdmin(admin.ModelAdmin):
    pass


class TaskAssignmentAdmin(admin.ModelAdmin):
    pass


class MessageAdmin(admin.ModelAdmin):
    pass


class TaskAssignmentResultAdmin(admin.ModelAdmin):
    pass


class FormFieldAdmin(admin.ModelAdmin):
    pass


admin.site.register(Task, TaskAdmin)
admin.site.register(Location, LocationAdmin)
admin.site.register(Store, StoreAdmin)
admin.site.register(Project, ProjectAdmin)
admin.site.register(TaskAssignment, TaskAssignmentAdmin)
admin.site.register(Message, MessageAdmin)
admin.site.register(TaskAssignmentResult, TaskAssignmentResultAdmin)
admin.site.register(FormField, FormFieldAdmin)
