from django.template.loader import get_template
from django.http import HttpResponseRedirect, HttpResponse
from django.http import JsonResponse
from django.core import serializers
from django.db.models import Count, Avg
from django.views.generic.edit import UpdateView, CreateView, DeleteView
from django.core.files.storage import FileSystemStorage
from django.contrib.gis.geoip2 import GeoIP2
from django.contrib.gis.geos import Point
from django.views.generic import TemplateView
from rest_framework import viewsets
from app.serializers import TaskFormFieldsSerializer, TaskSerializer
from app.models import (
    TaskFormResponse,
    TaskAssignmentResult,
    TaskFormFields,
    AvancesProyectoView,
    AsignacionesDeTareas,
    Task,
    TaskStore,
    TaskEvidence,
    TaskAssignment,
    OTWSystemLog,
)
from audits.models import (
    AuditProgrammedStoreVisit,
    AuditStoreVisit,
    AuditStoreVisitFieldResults,)
from store.models import (
    Store,
    )
from project.models import Project
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.forms import formset_factory
from django.urls import reverse_lazy
from django.views.generic.list import ListView
import datetime
from django.db.models import Q
from store.models import Store, Location
import simplejson as json



def log_out(request):
    """  Logout User """
    logout(request)
    return HttpResponseRedirect("/", request)


@method_decorator(login_required, name='dispatch')
class TaskExecuteDocOperatorUpdate(UpdateView):
    """ render update view document or photo task """
    model = TaskAssignmentResult
    fields = ('id','result','date','upload_one','upload_two','upload_three','audit_date','task_assignment')
    template_name = "operator/task_execute_document.html"

    def get_success_url(self):
        return reverse_lazy('dashboard')

    def get_context_data(self, **kwargs):
        context = (
            super(TaskExecuteDocOperatorUpdate, self)
            .get_context_data(**kwargs)
        )
        ta = TaskAssignmentResult.objects.get(pk=self.kwargs['pk'],)
        context['ta'] = ta.task_assignment
        context['tar'] = ta
        upload_one_ft = str(ta.upload_one).split(".")[-1]
        upload_two_ft = str(ta.upload_two).split(".")[-1]
        upload_three_ft = str(ta.upload_three).split(".")[-1]

        if upload_one_ft in (
            'jpg', 'jpeg', 'png', 'bmp', 'JPG', 'JPEG', 'PNG', 'BMP',
        ):
            context['upload_one_ft'] = "image"
        else:
            context['upload_one_ft'] = "doc"

        if upload_two_ft in (
            'jpg', 'jpeg', 'png', 'bmp', 'JPG', 'JPEG', 'PNG', 'BMP',
        ):
            context['upload_two_ft'] = "image"
        else:
            context['upload_two_ft'] = "doc"

        if upload_three_ft in (
            'jpg', 'jpeg', 'png', 'bmp', 'JPG', 'JPEG', 'PNG', 'BMP',
        ):
            context['upload_three_ft'] = "image"
        else:
            context['upload_three_ft'] = "doc"
        return context


@method_decorator(login_required, name='dispatch')
class TaskCreateNewField(CreateView):
    model = TaskFormFields
    fields = '__all__'
    template_name = 'tasks/create_new_form_field.html'


@method_decorator(login_required, name='dispatch')
class TaskExecuteDocOperatorNew(CreateView):
    """ render new view document or photo task """
    model = TaskAssignmentResult
    fields = ('upload_one', 'upload_two', 'upload_three', 'task_assignment',)
    template_name = "operator/task_execute_document.html"

    def get_success_url(self):
        return reverse_lazy('dashboard')

    def get_context_data(self, **kwargs):
        context = (
            super(TaskExecuteDocOperatorNew, self).get_context_data(**kwargs)
        )
        context['ta'] = (
            TaskAssignment.objects.get(pk=self.kwargs['pk'],)
        )
        context['upload_one_ft'] = "doc"
        context['upload_two_ft'] = "doc"
        context['upload_three_ft'] = "doc"
        context['new'] = True
        return context

    def form_valid(self, form, **kwargs):
        try:
            g = GeoIP2()
            ip = self.request.META.get('REMOTE_ADDR', None)
            lat_lon = g.lat_lon(ip)
        except Exception:
            lat_lon = (4.6492, -74.0628)
        self.object = form.save()
        self.object.coords = Point(lat_lon)
        self.object.save()
        ta = TaskAssignment.objects.get(pk=self.kwargs['pk'],)
        children = Task.objects.filter(
            parent=ta.task
        ).first()
        if (children):
            assignment = TaskAssignment.objects.filter(
                store=ta.store,
                task=children
            ).first()
  
            return HttpResponseRedirect(
                reverse_lazy('task-execute-doc-new', kwargs={
                    'pk': assignment.id
                })
            )

        return HttpResponseRedirect(self.get_success_url())


@method_decorator(login_required, name='dispatch')
class TaskExecuteFormOperatorUpdate(UpdateView):
    """ render update view form """
    model = TaskAssignmentResult
    fields = ['task_assignment', 'result']
    template_name = "operator/task_execute_form.html"

    def get_success_url(self):
        return reverse_lazy('dashboard')

    def get_context_data(self, **kwargs):
        context = (
            super(TaskExecuteFormOperatorUpdate, self)
            .get_context_data(**kwargs)
        )
        tar = TaskAssignmentResult.objects.get(pk=self.kwargs['pk'])
        context['ta'] = tar.task_assignment
        context['fields'] = (
            TaskFormFields.objects
            .filter(task=context['ta'].task)
        )
        responses = {}
        for field in context['fields']:
            formResponse = (
                TaskFormResponse
                .objects.
                filter(task_assign=context['ta'], formField=field)[:1]
                .get()
            )
            responses[field.id] = formResponse.response
        context['responses'] = json.dumps(responses)
        return context

    

    def form_valid(self, form, **kwargs):
        try:
            g = GeoIP2()
            ip = self.request.META.get('REMOTE_ADDR', None)
            lat_lon = g.lat_lon(ip)
        except Exception:
            lat_lon = (4.6492, -74.0628)
        self.object = form.save()
        self.object.coords = Point(lat_lon)
        self.object.save()
        ta = TaskAssignment.objects.get(pk=self.kwargs['pk'],)
        children = Task.objects.filter(
            parent=ta.task
        ).first()
        if (children):
            assignment = TaskAssignment.objects.filter(
                store=ta.store,
                task=children
            ).first()

            return HttpResponseRedirect(
                reverse_lazy('task-execute-doc-new', kwargs={
                    'pk': assignment.id
                })
            )

        return HttpResponseRedirect(self.get_success_url())


    def post(self, request, **kwargs):
        request.POST = request.POST.copy()
        del request.POST['csrfmiddlewaretoken']
        request.POST['result'] = request.POST
        request.POST['task_assignment'] = self.kwargs['pk']

        return (
            super(TaskExecuteFormOperatorUpdate, self)
            .post(request, **kwargs)
        )


@method_decorator(login_required, name='dispatch')
class TaskExecuteFormOperatorNew(CreateView):
    """ render new view form """
    model = TaskAssignmentResult
    fields = ('result', 'task_assignment',)
    template_name = "operator/task_execute_form.html"

    def get_success_url(self):
        return reverse_lazy('dashboard')

    def get_context_data(self, **kwargs):
        context = (
            super(TaskExecuteFormOperatorNew, self)
            .get_context_data(**kwargs)
        )
        context['ta'] = TaskAssignment.objects.get(pk=self.kwargs['pk'],)
        context['fields'] = (
            TaskFormFields.objects.filter(task=context['ta'].task)
        )
        return context

    def post(self, request, **kwargs):
        request.POST = request.POST.copy()
        del request.POST['csrfmiddlewaretoken']
        task_assign = TaskAssignment.objects.get(pk=self.kwargs['pk'],)
        for key in request.POST:
            TaskFormResponse.objects.create(
                task_assign=task_assign,
                formField=TaskFormFields.objects.get(pk=key),
                response=request.POST[key]
            )
        request.POST['task_assignment'] = self.kwargs['pk']
        return super(TaskExecuteFormOperatorNew, self).post(request, **kwargs)

    
    def form_valid(self, form, **kwargs):
        try:
            g = GeoIP2()
            ip = self.request.META.get('REMOTE_ADDR', None)
            lat_lon = g.lat_lon(ip)
        except Exception:
            lat_lon = (4.6492, -74.0628)
        self.object = form.save()
        self.object.coords = Point(lat_lon)
        self.object.save()

        return HttpResponseRedirect(self.get_success_url())


def add_task_assignment_result_periodicity_filter(queryset, periodicity):
    queryset_copy = queryset.clone()
    if (periodicity == 'Daily'):
        return queryset_copy.filter(
            date__gt=datetime.data.today()
        )
    return queryset


def get_dashboard_operator_context(request):
    today = datetime.date.today().weekday()
    month_day_number = datetime.date.today().day

    daily_query = Q(
        Q(task__days__contains=str(today)) &
        Q(task__periodicity='Daily')
    )
    monthly_query = Q(
        Q(task__days__contains=str(month_day_number)) &
        Q(task__periodicity='Monthly')
    )
    unique_query = Q(
        Q(task__periodicity='Unique') &
        Q(task__start_date=datetime.date.today())
    )
    task_recurrent = TaskAssignment.objects.filter(
        Q(project__stores__manager=request.user.id),
        Q(task__active=True),
        Q(task__parent__isnull=True),
        Q(daily_query | monthly_query | unique_query),
    )
    task = ListTaskOperatorHelper(task_recurrent)
    todo = 0
    delay = 0

    for t in task:
        if not t['task'].is_complete():
            todo = todo + 1
        if t['state']['label'] == "Delayed":
            delay = delay + 1
        task_progress = t["task"].get_progress()
        if task_progress == 100:
            if (t["result"]["object"]):
                t['state']['detail_url'] = (
                    "task-execute-update-{}/{}"
                    .format(
                        t["TaskAssignment"].task.type,
                        t["result"]["object"].id
                    )
                )
            else:
                t['state']['detail_url'] = '/'
        elif task_progress == 0:
            t['state']['detail_url'] = (
                "task-execute-new-{}/{}"
                .format(
                    t["TaskAssignment"].task.type,
                    t["TaskAssignment"].id
                )
            )
        else:
            if t["TaskAssignment"].next_task_assignment():
                t['state']['detail_url'] = (
                    "task-execute-new-{}/{}"
                    .format(
                        t["TaskAssignment"].next_task_assignment().task.type,
                        t["TaskAssignment"].next_task_assignment().id
                    )
                )

    projects = (
        TaskAssignmentResult
        .objects
        .filter(store__manager=request.user.id)
        .values(
            'project__name',
            'store__name',
        )
        .annotate(tcount=Count('id'), tavg=Avg('audit_score'))
    )
    myProjects = (
        AvancesProyectoView.objects
        .filter(store__manager=request.user.id)
        .values(
            'proyecto',
            'proyecto__name',
            'tienda',
        )
    )

    Task_structure = {}
    Task_children_structure = {}
    for item in myProjects:
        Task_parents = (
            Task.objects.filter(assignment__project_id= item["proyecto"])
            .values(
            'name',
            'id',
            'store'
            )
        )
           
        Task_structure[item["proyecto"]] = Task_parents

    SingleTasks =  (
        AsignacionesDeTareas.objects
        .filter(store__manager=request.user.id, assignment_to_project_id__isnull=True)
        .exclude(assignment_to_store_id__isnull=True)
        .values(
            'task_id',
            'task__name',
            'store__name',
            'store__id',
        )
    )


        
        #for father in Task_parents:
        #    Task_children = (
        #        Task.objects.filter(parent_id=father["id"],  assignment__project_id= item["proyecto"])
        #            .values(
        #            'name',
        #            'id',
        #            'store'
        #            ) 
        #        )
        #    Task_children_structure[father["id"]] = Task_children

    return {
        'task_recurrent': task,
        'task_count': len(task),
        'task_todo':  todo,
        'task_delay': delay,
        'projects': projects,
        'myProjects': myProjects,
        'task_structure': Task_structure,
        'single_tasks': SingleTasks,
        #'task_children_structure': Task_children_structure,
    }

def get_dashboard_auditor_context(request):
    item = AuditProgrammedStoreVisit.objects.filter(audit_record_visit__isnull=True).values('id', 'date_time_programmed','store')
   
    today = datetime.date.today().weekday()
    month_day_number = datetime.date.today().day

    type_task = ""
    listTask3 = []
    returnItem = []
    for t in item:
        type_task = "bg-success"
        theStore = Store.objects.filter(id=t["id"]).values('name')
        for Dt in theStore:
            name = Dt["name"]
            
        listTask3.append({
                    'id': t["id"],
                    'title': str(name),
                    'start': str(t["date_time_programmed"]),
                    'className': type_task,
                    'period': 'Unique',
                    'cache': False
                })  
        returnItem =  json.dumps(
            list(listTask3),
            separators=(',', ':')
        )



    daily_query = Q(
        Q(task__days__contains=str(today)) &
        Q(task__periodicity='Daily')
    )
    monthly_query = Q(
        Q(task__days__contains=str(month_day_number)) &
        Q(task__periodicity='Monthly')
    )
    unique_query = Q(
        Q(task__periodicity='Unique') &
        Q(task__start_date=datetime.date.today())
    )
    task_recurrent = TaskAssignment.objects.filter(
        Q(project__stores__auditor=request.user.id),
        Q(task__active=True),
        Q(daily_query | monthly_query | unique_query)
    )
    todo = 0
    delay = 0
    task = ListTaskOperatorHelper(task_recurrent)
    for t in task:
        if t['state']['label'] != "Complete":
            todo = todo + 1
        if t['state']['label'] == "Delayed":
            delay = delay + 1
        if t["result"]["status"] and 'object' in t["result"]:
            t['state']['detail_url'] = (
                "task-execute-update-{}/{}".format(
                    t["TaskAssignment"].task.type,
                    t["result"]["object"].id
                )
            )
        else:
            t['state']['detail_url'] = (
                "task-execute-new-{}/{}".format(
                    t["TaskAssignment"].task.type,
                    t["TaskAssignment"].id
                ),
            )
    projects = (
        TaskAssignmentResult.objects.filter(
            project__stores__manager=request.user.id
        ).values(
            'project__name',
            'project__stores__name',
        )
        .annotate(tcount=Count('id'), tavg=Avg('audit_score'))
    )
    return {
        'task_recurrent': task,
        'task_count': len(task),
        'canlendar_obj':returnItem
    }


def get_dashboard_internal_auditor_context(request):
    context = {}
    stores_of_client = Store.objects.filter(manager__client=request.user.client)
    ta = TaskAssignmentResult.objects.filter(
        store__in=stores_of_client,
    )
    context['assign_result'] = ta
    return context


@login_required(login_url='/login/')
def dashboard(request):
    """ dashboard for user """
    task_recurrent = {}
    context = {}
    month_day_number = datetime.date.today().day
    today = datetime.date.today().weekday()

    if request.user.get_profile() == 'Operator':
        context = get_dashboard_operator_context(request)

    if request.user.get_profile() == "Auditor":
        context = get_dashboard_auditor_context(request)

    if request.user.get_profile() == "Auditor interno":
        context = get_dashboard_internal_auditor_context(request)

    template = get_template('dashboard/dashboard.html')
    return HttpResponse(template.render(context, request))




@login_required(login_url='/login/')
def calendar_operator(request):
    """ profile for user """
    template = get_template('operator/calendar.html')
    task_recurrent = (
        TaskAssignment.objects.filter(
            store__manager=request.user.id
        )
    )
    task = ListTaskCalendarJsonHelper(task_recurrent)
    context = {
     'task': task,
    }
    return HttpResponse(template.render(context, request))



@method_decorator(login_required, name='dispatch')
class UpdateRatingTaskView(UpdateView):
    """ render update view document or photo task """
    model = TaskAssignmentResult
    fields = ['audit_result', 'audit_score']
    template_name = "dashboard/update-rating-task.html"

    def get_success_url(self):
        return '/'

    def get_context_data(self, **kwargs):
        context = super(UpdateRatingTaskView, self).get_context_data(**kwargs)
        return context

    def post(self, request, **kwargs):
        request.POST = request.POST.copy()
        request.POST["type"] = "Message"
        request.POST["audit_date"] = datetime.datetime.now()
        return super(UpdateRatingTaskView, self).post(request, **kwargs)

    def form_valid(self, form, **kwargs):
        try:
            g = GeoIP2()
            ip = self.request.META.get('REMOTE_ADDR', None)
            lat_lon = g.lat_lon(ip)
        except Exception:
            lat_lon = (4.6492, -74.0628)
        self.object = form.save()
        self.object.coords = Point(lat_lon)
        self.object.save()

        return HttpResponseRedirect(self.get_success_url())


@method_decorator(login_required, name='dispatch')
class CreateRatingTaskView(CreateView):
    """ render update view document or photo task """
    model = TaskAssignmentResult
    fields = '__all__'
    template_name = "dashboard/update-rating-task.html"

    def get_success_url(self):
        return '/'

    def get_context_data(self, **kwargs):
        context = super(CreateRatingTaskView, self).get_context_data(**kwargs)
        return context

    def post(self, request, **kwargs):
        request.POST = request.POST.copy()
        request.POST["type"] = "Message"

        del request.POST['csrfmiddlewaretoken']
        request.POST["task_assignment"] = kwargs['pk']
        request.POST["result"] = 'asdfasdf'
        request.POST["audit_date"] = datetime.datetime.now()
        request.POST["latitude"] = 0
        request.POST["longitude"] = 0
        return super(CreateRatingTaskView, self).post(request, **kwargs)


def ListTaskOperatorHelper(TaskAssignmentList):
    task_recurrent_list = []
    for t in TaskAssignmentList:
        state = {'state': 'Complete', 'label': 'label-success'}
        time_server = datetime.time(
            datetime.datetime.now().hour,
            datetime.datetime.now().minute
        )
        time_task_start = datetime.time(
            t.task.start_hour.hour,
            t.task.start_hour.minute,
        )
        time_task_end = datetime.time(
            t.task.end_hour.hour,
            t.task.end_hour.minute
        )
        if (time_server < time_task_start):
            state = {'label': 'Scheduled', 'class': 'label-warning'}
        elif (
            time_server > time_task_start and
            time_server < time_task_end
        ):
            state = {'label': 'Ejecutar tarea', 'class': 'label-info'}
        elif (time_server > time_task_end):
            state = {'label': 'Tarea retrasada', 'class': 'label-danger'}
        result = t.is_saved(datetime.datetime.now())
        if (t.task.get_progress() == 100):
            #task_assignment = t.taar_taa.all().order_by('-created_at').first()
            #if task_assignment:
            #    state = {'label': task_assignment.created_at.strftime("%d/%h %I:%M"), 'class': 'label-success'}
            #else:
            state = {'label': 'Tarea completa', 'class': 'label-success'}

        task_recurrent_list.append({
            'TaskAssignment': t,
            'hour': t.task.start_hour,
            'state': state,
            'task': t.task,
            'result': result},
        )
    return task_recurrent_list


def ListTaskCalendarJsonHelper(TaskAssignmentList):
    type_task = ""
    listTask = []
    for t in TaskAssignmentList:
        if not t.task:
            continue
        if t.task.type == 'document':
            type_task = "bg-success"
        if t.task.type == 'form':
            type_task = "bg-info"
        if t.task.type == 'survey':
            type_task = "bg-purple"
        if t.task.periodicity == "Daily":
            if (t.task.start_hour is None):
                start = datetime.datetime.now()
                end = datetime.datetime.now()
            else:
                listTask.append({
                    'id': t.id,
                    'title': '{}-({})'.format(t.task.name, t.store.name),
                    'start': str(t.task.start_hour),
                    'end': str(t.task.end_hour),
                    'allDay': True,
                    'className': type_task,
                    'period': 'daily',
                    'cache': False
                })
        elif t.task.periodicity == 'Monthly':
            today = datetime.datetime.now()
            listTask.append({
                'id': t.id,
                'title': '{}-({})'.format(t.task.name, t.store.name),
                'start': '{}-{}-{}'.format(today.year, today.month, t.task.day_number),
                'repeat': 1,
                'className': type_task,
                'period': 'monthly',
                'cache': False
            })
        elif t.task.periodicity == 'Unique':
            if t.task.start_date is not None and t.task.end_date is not None:
                start_date = datetime.datetime.combine(t.task.start_date, t.task.start_hour)
                end_date = datetime.datetime.combine(t.task.end_date, t.task.end_hour)
                listTask.append({
                    'id': t.id,
                    'title': '{}-({})'.format(t.task.name, t.store.name),
                    'start': str(start_date),
                    'end': str(end_date),
                    'className': type_task,
                    'period': 'Unique',
                    'cache': False
                })
    return json.dumps(
        list(listTask),
        separators=(',', ':')
    )



class TaskFormFieldsViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = TaskFormFields.objects.all()
    serializer_class = TaskFormFieldsSerializer


@login_required(login_url='/login/')
def reports_administrator(request):
    """ profile for user """
    # return HttpResponse(template.render(context, request))
    return HttpResponseRedirect('//axdigital.co:3000/')


@login_required(login_url='/login/')
def task_administrator(request):
    """ profile for user """
    template = get_template('administrator/task-administrator.html')
    task = Task.objects.filter(owner=request.user.id).order_by('-created_at')
    context = {
        'task': task,
    }
    return HttpResponse(template.render(context, request))


@method_decorator(login_required, name='dispatch')
class TaskAdministratorUpdateDoc(UpdateView):
    """ render update view form """
    model = Task
    fields = '__all__'
    template_name = "administrator/task-administrator-document.html"

    def get_success_url(self):
        if (self.object.assignment.count() == 0):
            return reverse_lazy('task_assign', kwargs={'pk': self.object.id})
        return reverse_lazy('task_administrator')

    def get_context_data(self, **kwargs):
        context = (
            super(TaskAdministratorUpdateDoc, self)
            .get_context_data(**kwargs)
        )
        project = Project.objects.filter(active=True,)
        parent = Task.objects.values(
            "name",
            "id",
            "parent__id",
        ).filter(
            active=True,
            owner__client=self.request.user.client,
        )

        fields = TaskFormFields.objects.filter(task=context['object']).values()

        context['parent_task'] = parent
        context['fields'] = json.dumps(list(fields))
        context['update'] = True
        return context

    def form_valid(self, form):
        request = self.request
        self.object = form.save()
        fields = request.POST.getlist('form_fields[]')
        for jsonField in fields:
            field = json.loads(jsonField)
            if ('id' not in field):
                TaskFormFields.objects.create(**field, task=self.object)
        return HttpResponseRedirect(self.get_success_url())

    def post(self, request, **kwargs):
        request.POST = request.POST.copy()
        days = ""
        week = ""
        for d in request.POST.getlist('days'):
            days += d+","
        for d in request.POST.getlist('week'):
            week += d+","
        request.POST['days'] = days
        request.POST['week'] = week
        return super(TaskAdministratorUpdateDoc, self).post(request, **kwargs)


class AssignTask(ListView):
    paginate_by = 100  # if pagination is desired
    template_name = 'administrator/assign_task.html'

    def get_queryset(self):
        return Store.objects.all()

    def get_context_data(self, **kwargs):
        task = Task.objects.get(pk=self.kwargs['pk'])
        stores = TaskStore.objects.filter(task=task).values('store_id')
        stores_ids = map(lambda x:x['store_id'], stores)
        context = super(AssignTask, self).get_context_data()
        context['countries'] = json.dumps(list(Location.objects.filter(type="Country").values('name', 'id')))
        context['zones'] = json.dumps(list(Location.objects.filter(type="Zone").values('name', 'id')))
        context['asigned_stores'] = list(stores_ids)
        return context

    def post(self, request, **kwargs):
        request.POST = request.POST.copy()
        assing_to_store = request.POST.getlist('assing_to_store')
        for store_id in assing_to_store:
            store = Store.objects.get(pk=store_id)
            task = Task.objects.get(pk=kwargs['pk'])
            TaskStore.objects.create(store=store, task=task)
        return HttpResponseRedirect(reverse_lazy('task_administrator'))


@method_decorator(login_required, name='dispatch')
class TaskAdministratorNewDoc(CreateView):
    """ render update view document or photo task """
    model = Task
    fields = '__all__'
    template_name = "administrator/task-administrator-document.html"

    def get_success_url(self):
        is_audit = self.request.GET.get('is_audit', None) == 'true'
        if (is_audit):
            return reverse_lazy('audits')
        return reverse_lazy('task_assign', kwargs={'pk': self.object.id})

    def get_context_data(self, **kwargs):
        context = (
            super(TaskAdministratorNewDoc, self)
            .get_context_data(**kwargs)
        )
        is_audit = self.request.GET.get('is_audit', None) == 'true'
        project = Project.objects.filter()
        context['projects'] = project
        context['is_audit'] = is_audit
        newFieldFormset = formset_factory(TaskCreateNewField)
        formset = newFieldFormset()
        context['parent_task'] = formset
        return context


    def form_valid(self, form):
        request = self.request
        self.object = form.save()
        fields = request.POST.getlist('form_fields[]')
        is_audit = request.GET.get('is_audit', None) == 'true'
        for jsonField in fields:
            field = json.loads(jsonField)
            TaskFormFields.objects.create(**field, task=self.object)
        if is_audit:
            stores = Store.objects.filter(auditor=request.user)
            for store in stores:
                TaskAssignment.objects.create(store=store, task=self.object)
        if self.request.is_ajax():
            return JsonResponse(TaskSerializer(self.object).data)
        return HttpResponseRedirect(self.get_success_url())

    def post(self, request, **kwargs):
        request.POST = request.POST.copy()
        is_audit = request.GET.get('is_audit', None) == 'true'
        days = ""
        week = ""

        for d in request.POST.getlist('days'):
            days += d+","
        for d in request.POST.getlist('week'):
            week += d+","
        request.POST['days'] = days
        request.POST['week'] = week
        request.POST['is_audit'] = is_audit
        return super(TaskAdministratorNewDoc, self).post(request, **kwargs)


class TaskAdministratorUpdateDocModal(TaskAdministratorNewDoc):
    template_name = "administrator/task-administrator-document-modal.html"

    def form_invalid(self, form):
        response = super().form_invalid(form)
        if self.request.is_ajax():
            return JsonResponse(form.errors, status=400)
        else:
            return response


class Help(TemplateView):
    template_name = "help.html"


class HelpOperator(TemplateView):
    template_name = "help_operator.html"
    
    
class TaskDelete(DeleteView):
    model = Task
    success_url = reverse_lazy('task_administrator')


class StoreTaskExecute(TemplateView):
    success_url = reverse_lazy('dashboard')
    template_name = 'tasks/execute_task.html'

    def get_context_data(self, **kwargs):
        st_id = self.kwargs['store_id']
        t_id = self.kwargs['task_id']

        task = Task.objects.get(pk=t_id)
        return {
            'task': json.dumps(TaskSerializer(task).data),
        }

    def post(self, request, **kwargs):
        request.POST = request.POST.copy()
        request.POST.pop('csrfmiddlewaretoken')
        st_id = self.kwargs['store_id']
        t_id = self.kwargs['task_id']
        project_id = self.kwargs['project_id']

        result = TaskAssignmentResult.objects.create(
            task_id=t_id,
            store_id=st_id,
            project_id=project_id
        )

        if result.task.type == 'form':
            for field_id in request.POST:
                TaskFormResponse.objects.create(
                    assignment_result=result,
                    formField_id=field_id,
                    response=request.POST[field_id]
                )
        else:
            for evidence in request.POST.getlist('files[]'):
                TaskEvidence.objects.create(
                    assignment_result=result,
                    evidence=evidence,
                )

        OTWSystemLog.objects.create(
            who=request.user,
            action="tarea ejecutada",
            details=t_id,
            readable_details=result.task.name
        )
        return HttpResponseRedirect(self.success_url)


def upload_task_evidence(request):
    fs = FileSystemStorage(location='static/files/task_evidences')
    uploaded_file = request.FILES['file']
    image_url = fs.save(uploaded_file.name, uploaded_file)
    return HttpResponse(image_url)


class TaskAudit(TemplateView):
    template_name = 'tasks/task_audit.html'
    success_url = reverse_lazy('dashboard')

    def get_context_data(self, **kwargs):
        result = TaskAssignmentResult.objects.get(pk=self.kwargs['result_id'])
        context = {}
        context['result'] = result
        return context

    def post(self, request, **kwargs):
        request.POST = request.POST.copy()
        result = TaskAssignmentResult.objects.get(pk=self.kwargs['result_id'])
        result.audit_score = request.POST['score']
        result.audit_result = request.POST['comments']
        result.audit_date = datetime.datetime.now()
        result.save()
        return HttpResponseRedirect(self.success_url)
from django.template.loader import get_template
from django.http import HttpResponseRedirect, HttpResponse
import os
from django.http import JsonResponse
from django.core import serializers
from django.db.models import Count, Avg
from django.views.generic.edit import UpdateView, CreateView, DeleteView
from django.core.files.storage import FileSystemStorage
from django.contrib.gis.geoip2 import GeoIP2
from django.contrib.gis.geos import Point
from django.views.generic import TemplateView
from rest_framework import viewsets
from app.serializers import TaskFormFieldsSerializer, TaskSerializer
from app.models import (
    TaskFormResponse,
    TaskAssignmentResult,
    TaskFormFields,
    AvancesProyectoView,
    Task,
    TaskStore,
    TaskEvidence,
    TaskAssignment,
    OTWSystemLog,
)
from audits.models import (
    AuditProgrammedStoreVisit,
    AuditStoreVisit,
    AuditStoreVisitFieldResults,)
from store.models import (
    Store,
    )
from project.models import Project
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.forms import formset_factory
from django.urls import reverse_lazy
from django.views.generic.list import ListView
import datetime
from django.db.models import Q
from store.models import Store, Location
import simplejson as json



def log_out(request):
    """  Logout User """
    logout(request)
    return HttpResponseRedirect("/", request)


@method_decorator(login_required, name='dispatch')
class TaskExecuteDocOperatorUpdate(UpdateView):
    """ render update view document or photo task """
    model = TaskAssignmentResult
    fields = ('id','result','date','upload_one','upload_two','upload_three','audit_date','task_assignment')
    template_name = "operator/task_execute_document.html"

    def get_success_url(self):
        return reverse_lazy('dashboard')

    def get_context_data(self, **kwargs):
        context = (
            super(TaskExecuteDocOperatorUpdate, self)
            .get_context_data(**kwargs)
        )
        ta = TaskAssignmentResult.objects.get(pk=self.kwargs['pk'],)
        context['ta'] = ta.task_assignment
        context['tar'] = ta
        upload_one_ft = str(ta.upload_one).split(".")[-1]
        upload_two_ft = str(ta.upload_two).split(".")[-1]
        upload_three_ft = str(ta.upload_three).split(".")[-1]

        if upload_one_ft in (
            'jpg', 'jpeg', 'png', 'bmp', 'JPG', 'JPEG', 'PNG', 'BMP',
        ):
            context['upload_one_ft'] = "image"
        else:
            context['upload_one_ft'] = "doc"

        if upload_two_ft in (
            'jpg', 'jpeg', 'png', 'bmp', 'JPG', 'JPEG', 'PNG', 'BMP',
        ):
            context['upload_two_ft'] = "image"
        else:
            context['upload_two_ft'] = "doc"

        if upload_three_ft in (
            'jpg', 'jpeg', 'png', 'bmp', 'JPG', 'JPEG', 'PNG', 'BMP',
        ):
            context['upload_three_ft'] = "image"
        else:
            context['upload_three_ft'] = "doc"
        return context


@method_decorator(login_required, name='dispatch')
class TaskCreateNewField(CreateView):
    model = TaskFormFields
    fields = '__all__'
    template_name = 'tasks/create_new_form_field.html'


@method_decorator(login_required, name='dispatch')
class TaskExecuteDocOperatorNew(CreateView):
    """ render new view document or photo task """
    model = TaskAssignmentResult
    fields = ('upload_one', 'upload_two', 'upload_three', 'task_assignment',)
    template_name = "operator/task_execute_document.html"

    def get_success_url(self):
        return reverse_lazy('dashboard')

    def get_context_data(self, **kwargs):
        context = (
            super(TaskExecuteDocOperatorNew, self).get_context_data(**kwargs)
        )
        context['ta'] = (
            TaskAssignment.objects.get(pk=self.kwargs['pk'],)
        )
        context['upload_one_ft'] = "doc"
        context['upload_two_ft'] = "doc"
        context['upload_three_ft'] = "doc"
        context['new'] = True
        return context

    def form_valid(self, form, **kwargs):
        try:
            g = GeoIP2()
            ip = self.request.META.get('REMOTE_ADDR', None)
            lat_lon = g.lat_lon(ip)
        except Exception:
            lat_lon = (4.6492, -74.0628)
        self.object = form.save()
        self.object.coords = Point(lat_lon)
        self.object.save()
        ta = TaskAssignment.objects.get(pk=self.kwargs['pk'],)
        children = Task.objects.filter(
            parent=ta.task
        ).first()
        if (children):
            assignment = TaskAssignment.objects.filter(
                store=ta.store,
                task=children
            ).first()
  
            return HttpResponseRedirect(
                reverse_lazy('task-execute-doc-new', kwargs={
                    'pk': assignment.id
                })
            )

        return HttpResponseRedirect(self.get_success_url())


@method_decorator(login_required, name='dispatch')
class TaskExecuteFormOperatorUpdate(UpdateView):
    """ render update view form """
    model = TaskAssignmentResult
    fields = ['task_assignment', 'result']
    template_name = "operator/task_execute_form.html"

    def get_success_url(self):
        return reverse_lazy('dashboard')

    def get_context_data(self, **kwargs):
        context = (
            super(TaskExecuteFormOperatorUpdate, self)
            .get_context_data(**kwargs)
        )
        tar = TaskAssignmentResult.objects.get(pk=self.kwargs['pk'])
        context['ta'] = tar.task_assignment
        context['fields'] = (
            TaskFormFields.objects
            .filter(task=context['ta'].task)
        )
        responses = {}
        for field in context['fields']:
            formResponse = (
                TaskFormResponse
                .objects.
                filter(task_assign=context['ta'], formField=field)[:1]
                .get()
            )
            responses[field.id] = formResponse.response
        context['responses'] = json.dumps(responses)
        return context

    

    def form_valid(self, form, **kwargs):
        try:
            g = GeoIP2()
            ip = self.request.META.get('REMOTE_ADDR', None)
            lat_lon = g.lat_lon(ip)
        except Exception:
            lat_lon = (4.6492, -74.0628)
        self.object = form.save()
        self.object.coords = Point(lat_lon)
        self.object.save()
        ta = TaskAssignment.objects.get(pk=self.kwargs['pk'],)
        children = Task.objects.filter(
            parent=ta.task
        ).first()
        if (children):
            assignment = TaskAssignment.objects.filter(
                store=ta.store,
                task=children
            ).first()

            return HttpResponseRedirect(
                reverse_lazy('task-execute-doc-new', kwargs={
                    'pk': assignment.id
                })
            )

        return HttpResponseRedirect(self.get_success_url())


    def post(self, request, **kwargs):
        request.POST = request.POST.copy()
        del request.POST['csrfmiddlewaretoken']
        request.POST['result'] = request.POST
        request.POST['task_assignment'] = self.kwargs['pk']

        return (
            super(TaskExecuteFormOperatorUpdate, self)
            .post(request, **kwargs)
        )


@method_decorator(login_required, name='dispatch')
class TaskExecuteFormOperatorNew(CreateView):
    """ render new view form """
    model = TaskAssignmentResult
    fields = ('result', 'task_assignment',)
    template_name = "operator/task_execute_form.html"

    def get_success_url(self):
        return reverse_lazy('dashboard')

    def get_context_data(self, **kwargs):
        context = (
            super(TaskExecuteFormOperatorNew, self)
            .get_context_data(**kwargs)
        )
        context['ta'] = TaskAssignment.objects.get(pk=self.kwargs['pk'],)
        context['fields'] = (
            TaskFormFields.objects.filter(task=context['ta'].task)
        )
        return context

    def post(self, request, **kwargs):
        request.POST = request.POST.copy()
        del request.POST['csrfmiddlewaretoken']
        task_assign = TaskAssignment.objects.get(pk=self.kwargs['pk'],)
        for key in request.POST:
            TaskFormResponse.objects.create(
                task_assign=task_assign,
                formField=TaskFormFields.objects.get(pk=key),
                response=request.POST[key]
            )
        request.POST['task_assignment'] = self.kwargs['pk']
        return super(TaskExecuteFormOperatorNew, self).post(request, **kwargs)

    
    def form_valid(self, form, **kwargs):
        try:
            g = GeoIP2()
            ip = self.request.META.get('REMOTE_ADDR', None)
            lat_lon = g.lat_lon(ip)
        except Exception:
            lat_lon = (4.6492, -74.0628)
        self.object = form.save()
        self.object.coords = Point(lat_lon)
        self.object.save()

        return HttpResponseRedirect(self.get_success_url())


def add_task_assignment_result_periodicity_filter(queryset, periodicity):
    queryset_copy = queryset.clone()
    if (periodicity == 'Daily'):
        return queryset_copy.filter(
            date__gt=datetime.data.today()
        )
    return queryset


def get_dashboard_operator_context(request):
    today = datetime.date.today().weekday()
    month_day_number = datetime.date.today().day

    daily_query = Q(
        Q(task__days__contains=str(today)) &
        Q(task__periodicity='Daily')
    )
    monthly_query = Q(
        Q(task__days__contains=str(month_day_number)) &
        Q(task__periodicity='Monthly')
    )
    unique_query = Q(
        Q(task__periodicity='Unique') &
        Q(task__start_date=datetime.date.today())
    )
    task_recurrent = TaskAssignment.objects.filter(
        Q(project__stores__manager=request.user.id),
        Q(task__active=True),
        Q(task__parent__isnull=True),
        Q(daily_query | monthly_query | unique_query),
    )
    task = ListTaskOperatorHelper(task_recurrent)
    todo = 0
    delay = 0

    for t in task:
        if not t['task'].is_complete():
            todo = todo + 1
        if t['state']['label'] == "Delayed":
            delay = delay + 1
        task_progress = t["task"].get_progress()
        if task_progress == 100:
            if (t["result"]["object"]):
                t['state']['detail_url'] = (
                    "task-execute-update-{}/{}"
                    .format(
                        t["TaskAssignment"].task.type,
                        t["result"]["object"].id
                    )
                )
            else:
                t['state']['detail_url'] = '/'
        elif task_progress == 0:
            tienda = (Store.objects.filter(project__id= t["TaskAssignment"].project.id, project__task_assignments__task_id =  t["TaskAssignment"].task.id ).values(
                'id'
            ))
            
            t['state']['detail_url'] = (
                #"task-execute-new-{}/{}"
                "task-execute/{}/{}/{}"
                .format(
                    #t["TaskAssignment"].task.type,
                    #t["TaskAssignment"].id
                    tienda[0]["id"],
                    t["TaskAssignment"].task.id,
                    t["TaskAssignment"].project.id
                )
            )
        else:
            if t["TaskAssignment"].next_task_assignment():
                tienda = (Store.objects.filter(project__id= t["TaskAssignment"].project.id, project__task_assignments__task_id =  t["TaskAssignment"].task.id ).values(
                      'id'
                     ))
           
                t['state']['detail_url'] = (
                    #"task-execute-new-{}/{}"
                "task-execute/{}/{}/{}"
                    .format(
                    #    t["TaskAssignment"].next_task_assignment().task.type,
                    #    t["TaskAssignment"].next_task_assignment().id
                    tienda[0]["id"],
                    t["TaskAssignment"].next_task_assignment().task.id,
                    t["TaskAssignment"].next_task_assignment().project.id
                    )
                )

    projects = (
        TaskAssignmentResult
        .objects
        .filter(store__manager=request.user.id)
        .values(
            'project__name',
            'store__name',
        )
        .annotate(tcount=Count('id'), tavg=Avg('audit_score'))
    )
    myProjects = (
        AvancesProyectoView.objects
        .filter(store__manager=request.user.id)
        .values(
            'proyecto',
            'proyecto__name',
            'tienda',
        )
    )

    Task_structure = {}
    Task_children_structure = {}
    for item in myProjects:
        Task_parents = (
            Task.objects.filter(assignment__project_id= item["proyecto"])
            .values(
            'name',
            'id',
            'assignment__project__stores__id'
            )
        )
           
        Task_structure[item["proyecto"]] = Task_parents

    

    SingleTask =  (
        AsignacionesDeTareas.objects
        .filter(store__manager=request.user.id, project_id__isnull=True)
        .values(
            'task_id',
            'task__name',
            'store_id',
        )
    )
    #raise Exception(SingleTask)

        
        #for father in Task_parents:
        #    Task_children = (
        #        Task.objects.filter(parent_id=father["id"],  assignment__project_id= item["proyecto"])
        #            .values(
        #            'name',
        #            'id',
        #            'store'
        #            ) 
        #        )
        #    Task_children_structure[father["id"]] = Task_children

    #raise Exception(Task_structure)
    return {
        'task_recurrent': task,
        'task_count': len(task),
        'task_todo':  todo,
        'task_delay': delay,
        'projects': projects,
        'myProjects': myProjects,
        'task_structure': Task_structure,
        'single_tasks': SingleTask,
        #'task_children_structure': Task_children_structure,
    }

def get_dashboard_auditor_context(request):
    item = AuditProgrammedStoreVisit.objects.filter(audit_record_visit__isnull=True).values('id', 'date_time_programmed','store')
   
    today = datetime.date.today().weekday()
    month_day_number = datetime.date.today().day

    type_task = ""
    listTask3 = []
    returnItem = []
    for t in item:
        type_task = "bg-success"
        theStore = Store.objects.filter(id=t["id"]).values('name')
        for Dt in theStore:
            name = Dt["name"]
            
        listTask3.append({
                    'id': t["id"],
                    'title': str(name),
                    'start': str(t["date_time_programmed"]),
                    'className': type_task,
                    'period': 'Unique',
                    'cache': False
                })  
        returnItem =  json.dumps(
            list(listTask3),
            separators=(',', ':')
        )



    daily_query = Q(
        Q(task__days__contains=str(today)) &
        Q(task__periodicity='Daily')
    )
    monthly_query = Q(
        Q(task__days__contains=str(month_day_number)) &
        Q(task__periodicity='Monthly')
    )
    unique_query = Q(
        Q(task__periodicity='Unique') &
        Q(task__start_date=datetime.date.today())
    )
    task_recurrent = TaskAssignment.objects.filter(
        Q(project__stores__auditor=request.user.id),
        Q(task__active=True),
        Q(daily_query | monthly_query | unique_query)
    )
    todo = 0
    delay = 0
    task = ListTaskOperatorHelper(task_recurrent)
    for t in task:
        if t['state']['label'] != "Complete":
            todo = todo + 1
        if t['state']['label'] == "Delayed":
            delay = delay + 1
        if t["result"]["status"] and 'object' in t["result"]:
            t['state']['detail_url'] = (
                "task-execute-update-{}/{}".format(
                    t["TaskAssignment"].task.type,
                    t["result"]["object"].id
                )
            )
        else:
            t['state']['detail_url'] = (
                "task-execute-new-{}/{}".format(
                    t["TaskAssignment"].task.type,
                    t["TaskAssignment"].id
                ),
            )
    projects = (
        TaskAssignmentResult.objects.filter(
            project__stores__manager=request.user.id
        ).values(
            'project__name',
            'project__stores__name',
        )
        .annotate(tcount=Count('id'), tavg=Avg('audit_score'))
    )
    return {
        'task_recurrent': task,
        'task_count': len(task),
        'canlendar_obj':returnItem
    }


def get_dashboard_internal_auditor_context(request):
    context = {}
    stores_of_client = Store.objects.filter(manager__client=request.user.client)
    ta = TaskAssignmentResult.objects.filter(
        store__in=stores_of_client,
    )
    context['assign_result'] = ta
    return context


@login_required(login_url='/login/')
def dashboard(request):
    """ dashboard for user """
    task_recurrent = {}
    context = {}
    month_day_number = datetime.date.today().day
    today = datetime.date.today().weekday()

    if request.user.get_profile() == 'Operator':
        context = get_dashboard_operator_context(request)

    if request.user.get_profile() == "Auditor":
        context = get_dashboard_auditor_context(request)

    if request.user.get_profile() == "Administrator":
        context = { 'metabase_server': os.environ.get('METABASE_SERVER') }
    if request.user.get_profile() == "Auditor interno":
        context = get_dashboard_internal_auditor_context(request)

    template = get_template('dashboard/dashboard.html')
    return HttpResponse(template.render(context, request))




@login_required(login_url='/login/')
def calendar_operator(request):
    """ profile for user """
    template = get_template('operator/calendar.html')
    task_recurrent = (
        TaskAssignment.objects.filter(
            project__stores__manager=request.user.id
        )
    )
    task = ListTaskCalendarJsonHelper(task_recurrent)
    context = {
     'task': task,
    }
    return HttpResponse(template.render(context, request))



@method_decorator(login_required, name='dispatch')
class UpdateRatingTaskView(UpdateView):
    """ render update view document or photo task """
    model = TaskAssignmentResult
    fields = ['audit_result', 'audit_score']
    template_name = "dashboard/update-rating-task.html"

    def get_success_url(self):
        return '/'

    def get_context_data(self, **kwargs):
        context = super(UpdateRatingTaskView, self).get_context_data(**kwargs)
        return context

    def post(self, request, **kwargs):
        request.POST = request.POST.copy()
        request.POST["type"] = "Message"
        request.POST["audit_date"] = datetime.datetime.now()
        return super(UpdateRatingTaskView, self).post(request, **kwargs)

    def form_valid(self, form, **kwargs):
        try:
            g = GeoIP2()
            ip = self.request.META.get('REMOTE_ADDR', None)
            lat_lon = g.lat_lon(ip)
        except Exception:
            lat_lon = (4.6492, -74.0628)
        self.object = form.save()
        self.object.coords = Point(lat_lon)
        self.object.save()

        return HttpResponseRedirect(self.get_success_url())


@method_decorator(login_required, name='dispatch')
class CreateRatingTaskView(CreateView):
    """ render update view document or photo task """
    model = TaskAssignmentResult
    fields = '__all__'
    template_name = "dashboard/update-rating-task.html"

    def get_success_url(self):
        return '/'

    def get_context_data(self, **kwargs):
        context = super(CreateRatingTaskView, self).get_context_data(**kwargs)
        return context

    def post(self, request, **kwargs):
        request.POST = request.POST.copy()
        request.POST["type"] = "Message"

        del request.POST['csrfmiddlewaretoken']
        request.POST["task_assignment"] = kwargs['pk']
        request.POST["result"] = 'asdfasdf'
        request.POST["audit_date"] = datetime.datetime.now()
        request.POST["latitude"] = 0
        request.POST["longitude"] = 0
        return super(CreateRatingTaskView, self).post(request, **kwargs)


def ListTaskOperatorHelper(TaskAssignmentList):
    task_recurrent_list = []
    for t in TaskAssignmentList:
        state = {'state': 'Complete', 'label': 'label-success'}
        time_server = datetime.time(
            datetime.datetime.now().hour,
            datetime.datetime.now().minute
        )
        time_task_start = datetime.time(
            t.task.start_hour.hour,
            t.task.start_hour.minute,
        )
        time_task_end = datetime.time(
            t.task.end_hour.hour,
            t.task.end_hour.minute
        )
        if (time_server < time_task_start):
            state = {'label': 'Scheduled', 'class': 'label-warning'}
        elif (
            time_server > time_task_start and
            time_server < time_task_end
        ):
            state = {'label': 'Ejecutar tarea', 'class': 'label-info'}
        elif (time_server > time_task_end):
            state = {'label': 'Tarea retrasada', 'class': 'label-danger'}
        result = t.is_saved(datetime.datetime.now())
        if (t.task.get_progress() == 100):
            #ask_assignment = t.taar_taa.all().order_by('-created_at').first()
            #if task_assignment:
            #    state = {'label': task_assignment.created_at.strftime("%d/%h %I:%M"), 'class': 'label-success'}
            #else:
            state = {'label': 'Tarea completa', 'class': 'label-success'}

        task_recurrent_list.append({
            'TaskAssignment': t,
            'hour': t.task.start_hour,
            'state': state,
            'task': t.task,
            'result': result},
        )
    return task_recurrent_list


def ListTaskCalendarJsonHelper(TaskAssignmentList):
    type_task = ""
    listTask = []
    for t in TaskAssignmentList:
        if not t.task:
            continue
        if t.task.type == 'document':
            type_task = "bg-success"
        if t.task.type == 'form':
            type_task = "bg-info"
        if t.task.type == 'survey':
            type_task = "bg-purple"
        if t.task.periodicity == "Daily":
            if (t.task.start_hour is None):
                start = datetime.datetime.now()
                end = datetime.datetime.now()
            else:
                listTask.append({
                    'id': t.id,
                    'title': '{}-({})'.format(t.task.name, t.project.stores.name),
                    'start': str(t.task.start_hour),
                    'end': str(t.task.end_hour),
                    'allDay': True,
                    'className': type_task,
                    'period': 'daily',
                    'cache': False
                })
        elif t.task.periodicity == 'Monthly':
            today = datetime.datetime.now()
            listTask.append({
                'id': t.id,
                'title': '{}-({})'.format(t.task.name, t.store.name),
                'start': '{}-{}-{}'.format(today.year, today.month, t.task.day_number),
                'repeat': 1,
                'className': type_task,
                'period': 'monthly',
                'cache': False
            })
        elif t.task.periodicity == 'Unique':
            if t.task.start_date is not None and t.task.end_date is not None:
                start_date = datetime.datetime.combine(t.task.start_date, t.task.start_hour)
                end_date = datetime.datetime.combine(t.task.end_date, t.task.end_hour)
                listTask.append({
                    'id': t.id,
                    'title': '{}-({})'.format(t.task.name, t.store.name),
                    'start': str(start_date),
                    'end': str(end_date),
                    'className': type_task,
                    'period': 'Unique',
                    'cache': False
                })
    return json.dumps(
        list(listTask),
        separators=(',', ':')
    )



class TaskFormFieldsViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = TaskFormFields.objects.all()
    serializer_class = TaskFormFieldsSerializer


@login_required(login_url='/login/')
def reports_administrator(request):
    """ profile for user """
    # return HttpResponse(template.render(context, request))
    return HttpResponseRedirect('//axdigital.co:3000/')


@login_required(login_url='/login/')
def task_administrator(request):
    """ profile for user """
    template = get_template('administrator/task-administrator.html')
    task = Task.objects.filter(owner=request.user.id).order_by('-created_at')
    context = {
        'task': task,
    }
    return HttpResponse(template.render(context, request))


@method_decorator(login_required, name='dispatch')
class TaskAdministratorUpdateDoc(UpdateView):
    """ render update view form """
    model = Task
    fields = '__all__'
    template_name = "administrator/task-administrator-document.html"

    def get_success_url(self):
        if (self.object.assignment.count() == 0):
            return reverse_lazy('task_assign', kwargs={'pk': self.object.id})
        return reverse_lazy('task_administrator')

    def get_context_data(self, **kwargs):
        context = (
            super(TaskAdministratorUpdateDoc, self)
            .get_context_data(**kwargs)
        )
        parent = []
        tasks = Task.objects.values(
            "name",
            "id",
            "parent__id",
        ).filter(
            active=True,
        ).exclude(
            id=self.object.id
        )
        parent = list(map(lambda t: { 'name' : t['name'] }, tasks))

        fields = TaskFormFields.objects.filter(task=context['object']).values()

        context['parent_task'] = parent
        context['fields'] = json.dumps(list(fields))
        context['update'] = True
        return context

    def form_valid(self, form):
        request = self.request
        self.object = form.save()
        fields = request.POST.getlist('form_fields[]')
        for jsonField in fields:
            field = json.loads(jsonField)
            if ('id' not in field):
                TaskFormFields.objects.create(**field, task=self.object)
        return HttpResponseRedirect(self.get_success_url())

    def post(self, request, **kwargs):
        request.POST = request.POST.copy()
        days = ""
        week = ""
        for d in request.POST.getlist('days'):
            days += d+","
        for d in request.POST.getlist('week'):
            week += d+","
        request.POST['days'] = days
        request.POST['week'] = week
        return super(TaskAdministratorUpdateDoc, self).post(request, **kwargs)


class AssignTask(ListView):
    paginate_by = 100  # if pagination is desired
    template_name = 'administrator/assign_task.html'

    def get_queryset(self):
        return Store.objects.all()

    def get_context_data(self, **kwargs):
        task = Task.objects.get(pk=self.kwargs['pk'])
        stores = TaskStore.objects.filter(task=task).values('store_id')
        stores_ids = map(lambda x:x['store_id'], stores)
        context = super(AssignTask, self).get_context_data()
        context['countries'] = json.dumps(list(Location.objects.filter(type="Country").values('name', 'id')))
        context['zones'] = json.dumps(list(Location.objects.filter(type="Zone").values('name', 'id')))
        context['asigned_stores'] = list(stores_ids)
        return context

    def post(self, request, **kwargs):
        request.POST = request.POST.copy()
        assing_to_store = request.POST.getlist('assing_to_store')
        for store_id in assing_to_store:
            store = Store.objects.get(pk=store_id)
            task = Task.objects.get(pk=kwargs['pk'])
            TaskStore.objects.create(store=store, task=task)
        return HttpResponseRedirect(reverse_lazy('task_administrator'))


@method_decorator(login_required, name='dispatch')
class TaskAdministratorNewDoc(CreateView):
    """ render update view document or photo task """
    model = Task
    fields = '__all__'
    template_name = "administrator/task-administrator-document.html"

    def get_success_url(self):
        is_audit = self.request.GET.get('is_audit', None) == 'true'
        if (is_audit):
            return reverse_lazy('audits')
        return reverse_lazy('task_assign', kwargs={'pk': self.object.id})

    def get_context_data(self, **kwargs):
        context = (
            super(TaskAdministratorNewDoc, self)
            .get_context_data(**kwargs)
        )
        newFieldFormset = formset_factory(TaskCreateNewField)
        parent_task = Task.objects.filter(
            owner__client=self.request.user.client,
        )
        formset = newFieldFormset()
        context['new_field_form'] = formset
        context['parent_task'] = parent_task
        return context


    def form_valid(self, form):
        request = self.request
        self.object = form.save()
        fields = request.POST.getlist('form_fields[]')
        is_audit = request.GET.get('is_audit', None) == 'true'
        for jsonField in fields:
            field = json.loads(jsonField)
            TaskFormFields.objects.create(**field, task=self.object)
        if is_audit:
            stores = Store.objects.filter(auditor=request.user)
            for store in stores:
                TaskAssignment.objects.create(store=store, task=self.object)
        if self.request.is_ajax():
            return JsonResponse(TaskSerializer(self.object).data)
        return HttpResponseRedirect(self.get_success_url())

    def post(self, request, **kwargs):
        request.POST = request.POST.copy()
        is_audit = request.GET.get('is_audit', None) == 'true'
        days = ""
        week = ""

        for d in request.POST.getlist('days'):
            days += d+","
        for d in request.POST.getlist('week'):
            week += d+","
        request.POST['days'] = days
        request.POST['week'] = week
        request.POST['is_audit'] = is_audit
        return super(TaskAdministratorNewDoc, self).post(request, **kwargs)


class TaskAdministratorUpdateDocModal(TaskAdministratorNewDoc):
    template_name = "administrator/task-administrator-document-modal.html"

    def form_invalid(self, form):
        response = super().form_invalid(form)
        if self.request.is_ajax():
            return JsonResponse(form.errors, status=400)
        else:
            return response


class Help(TemplateView):
    template_name = "help.html"


class HelpOperator(TemplateView):
    template_name = "help_operator.html"
    
    
class TaskDelete(DeleteView):
    model = Task
    success_url = reverse_lazy('task_administrator')


class StoreTaskExecute(TemplateView):
    success_url = reverse_lazy('dashboard')
    template_name = 'tasks/execute_task.html'

    def get_context_data(self, **kwargs):
        st_id = self.kwargs['store_id']
        t_id = self.kwargs['task_id']

        task = Task.objects.get(pk=t_id)
        return {
            'task': json.dumps(TaskSerializer(task).data),
        }

    def post(self, request, **kwargs):
        request.POST = request.POST.copy()
        request.POST.pop('csrfmiddlewaretoken')
        st_id = self.kwargs['store_id']
        t_id = self.kwargs['task_id']
        project_id = self.kwargs['project_id']

        result = TaskAssignmentResult.objects.create(
            task_id=t_id,
            store_id=st_id,
            project_id=project_id
        )

        if result.task.type == 'form':
            for field_id in request.POST:
                TaskFormResponse.objects.create(
                    assignment_result=result,
                    formField_id=field_id,
                    response=request.POST[field_id]
                )
        else:
            for evidence in request.POST.getlist('files[]'):
                TaskEvidence.objects.create(
                    assignment_result=result,
                    evidence=evidence,
                )

        OTWSystemLog.objects.create(
            who=request.user,
            action="tarea ejecutada",
            details=t_id,
            readable_details=result.task.name
        )
        return HttpResponseRedirect(self.success_url)


def upload_task_evidence(request):
    fs = FileSystemStorage(location='static/files/task_evidences')
    uploaded_file = request.FILES['file']
    image_url = fs.save(uploaded_file.name, uploaded_file)
    return HttpResponse(image_url)


class TaskAudit(TemplateView):
    template_name = 'tasks/task_audit.html'
    success_url = reverse_lazy('dashboard')

    def get_context_data(self, **kwargs):
        result = TaskAssignmentResult.objects.get(pk=self.kwargs['result_id'])
        context = {}
        context['result'] = result
        return context

    def post(self, request, **kwargs):
        request.POST = request.POST.copy()
        result = TaskAssignmentResult.objects.get(pk=self.kwargs['result_id'])
        result.audit_score = request.POST['score']
        result.audit_result = request.POST['comments']
        result.audit_date = datetime.datetime.now()
        result.save()
        return HttpResponseRedirect(self.success_url)
