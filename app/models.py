from django.contrib.gis.db import models
from functools import reduce
from datetime import datetime, date
from django.core.exceptions import ObjectDoesNotExist
from django.template import RequestContext
from task.middlewares import RequestMiddleware
from datetime import datetime

TASK_TYPE_CHOICES = (
    ('document', 'Foto / Documento'),
    ('form', 'Formulario / Encuesta'),
)

TASK_PERIODICITY_CHOICES = (
    ('Daily', 'Diario'),
    ('Monthly', 'Mensual'),
    ('Unique', 'Único'),
)

TASK_DAY_NUMBER_CHOICES = (
    ('01', '01'),
    ('02', '02'),
    ('03', '03'),
    ('04', '04'),
    ('05', '05'),
    ('06', '06'),
    ('07', '07'),
    ('08', '08'),
    ('09', '09'),
    ('10', '10'),
    ('11', '11'),
    ('12', '12'),
    ('13', '13'),
    ('14', '14'),
    ('15', '15'),
    ('16', '16'),
    ('17', '17'),
    ('18', '18'),
    ('19', '19'),
    ('20', '20'),
    ('21', '21'),
    ('22', '22'),
    ('23', '23'),
    ('24', '24'),
    ('25', '25'),
    ('26', '26'),
    ('27', '27'),
    ('28', '28'),
    ('29', '29'),
    ('30', '30'),
    ('31', '31'),
    ('End of the month', 'End of the month'),
)

FORM_TYPE_CHOICES = (
    ('checkbox', 'Checkbox'),
    ('number', 'Número'),
    ('textarea', 'Texto largo'),
    ('radio', 'Selección múltiple'),
)

FORM_CLASS_CHOICES = (
    ('form-control', 'Normal'),
)

TASK_FORM_INPUT_TYPES = (
    ('open-ended-question', 'Open ended question'),
    ('multiple-choice', 'Multiple choise'),
    ('checkbox', 'checkbox'),
    ('number', 'number'),
)


class TaskStore(models.Model):
    store = models.ForeignKey('store.Store', on_delete=models.CASCADE)
    task = models.ForeignKey('Task', on_delete=models.CASCADE)
    

class Task(models.Model):
    """ Model task """
    name = models.CharField(
        max_length=50,
        null=False,
        blank=False,
        default=None,
        verbose_name="Nombre"
    )
    owner = models.ForeignKey(
        'user_profile.User',
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        verbose_name="Creado por"
    )
    periodicity = models.CharField(
        max_length=30,
        choices=TASK_PERIODICITY_CHOICES,
        verbose_name="Periodicidad"
    )
    type = models.CharField(max_length=30, choices=TASK_TYPE_CHOICES, verbose_name="Tipo de tarea",)
    # aplica para todos los tipo de periodicidad
    start_date = models.DateField(
        blank=True,
        null=True,
        verbose_name="Fecha de inicio"
    )
    end_date = models.DateField(
        blank=True,
        null=True,
        verbose_name="Fecha de finalización"
    )
    start_hour = models.TimeField(
        null=True,
        blank=True,
        default=None,
        verbose_name="Hora de inicio"
    )
    end_hour = models.TimeField(
        null=True,
        blank=True,
        default=None,
        verbose_name="Hora de finalización"
    )
    # Dias de la semana en la que se ejecuta una tarea empieza en lunes=0 y
    # esta separada por ","
    days = models.CharField(
        max_length=50,
        null=True,
        blank=True,
        default="0,1,2,3,4,5,6",
        verbose_name="Días"
    )
    week = models.CharField(
        max_length=50,
        null=True,
        blank=True,
        default="0,1,2,3",
        verbose_name="Semana"
    )
    # datos para tipo Monthly
    day_number = models.CharField(
        max_length=30,
        choices=TASK_DAY_NUMBER_CHOICES,
        null=True,
        verbose_name="Día",
        blank=True
    )
    active = models.BooleanField(
        default=True
    )
    parent = models.ForeignKey(
        'self',
        on_delete=models.SET_NULL,
        null=True,
        verbose_name="Ejecutar después de",
        blank=True,
        related_name='ta_task'
    )
    store = models.ManyToManyField(
        'store.Store',
        null=True,
        blank=True,
        verbose_name="Tienda",
        related_name='tasks'
    )
    created_at = models.DateTimeField(auto_now_add=True)
    instructions = models.TextField(blank=True, verbose_name="Instrucciones",)
    is_audit = models.BooleanField(default=False)
    
    def get_periodicity(self):
        return dict(TASK_PERIODICITY_CHOICES)[self.periodicity]
    
    def get_type(self):
        return dict(TASK_TYPE_CHOICES)[self.type] 
    
    def verbose_type(self):
        return dict(TASK_TYPE_CHOICES)[self.type] 

    def get_complete_percent(self):
        return '{}%'.format(int((self.get_total_completed_tasks() * 100) / self.get_total_tasks()))

    def get_score(self):
        childrens = self.ta_task.all()
        completed_tasks = 0
        request = RequestMiddleware(get_response=None)
        request = request.thread_local.current_request

        for children in childrens:
            if (children.is_complete(request)):
                completed_tasks += 1

        if completed_tasks == 0:
            return 0
        completed = int((completed_tasks * 100) / childrens.count())
        return '{}%'.format(completed)

    def projects_names(self):
        names = []
        return reduce(
            lambda prev, ta: "{}, {}".format(prev, ta.project.name) if prev else ta.project.name, 
            self.assignment.all(),
            ''
        )

    def get_duration(self):
        return (
            datetime.combine(date.today(), self.end_hour) -
            datetime.combine(date.today(), self.start_hour)
        )

    def get_instructions(self):
        if len(self.instructions) > 50:
            return self.instructions[:50] + "..."
        else:
            return self.instructions[:50]

    def is_complete(self):
        request = RequestMiddleware(get_response=None)
        request = request.thread_local.current_request
        return TaskAssignmentResult.objects.filter(
            task=self,
            store__manager=request.user
        ).exists()

    def get_total_tasks(self, count=1):
        if not self.ta_task:
            return count
        return reduce((lambda x, y: x + y.get_total_tasks()), self.ta_task.all(), count)

    def get_total_completed_tasks(self, count=None):
        if not self.ta_task:
            return count
        if count is None:
            count = 1 if self.is_complete() else 0

        return reduce(
            (lambda x, y: x + y.get_total_completed_tasks()),
            self.ta_task.all(),
            count
        )

    def get_progress(self):
        completed = self.get_total_completed_tasks()
        if (completed == 0):
            return 0
        total_tasks = self.get_total_tasks()

        return int((completed * 100) / total_tasks)

    class Meta:
        db_table = 'task'

    def __str__(self):
        return self.name


class TaskFormFields(models.Model):
    label = models.CharField(max_length=200)
    input_type = models.CharField(
        max_length=200,
        choices=TASK_FORM_INPUT_TYPES
    )
    options = models.TextField(blank=True)
    task = models.ForeignKey(
        to=Task,
        on_delete=models.CASCADE,
        related_name='formFields'
    )
    optional = models.BooleanField(default=False)

    def is_multiple(self):
        return self.input_type == 'multiple-choice'

    def options_array(self):
        return self.options.split(',')


class TaskFormResponse(models.Model):
    assignment_result = models.ForeignKey(
        to='TaskAssignmentResult',
        on_delete=models.CASCADE,
        related_name="form_responses"
    )
    formField = models.ForeignKey(
        to='TaskFormFields',
        on_delete=models.CASCADE
    )
    response = models.CharField(max_length=500)
    created_at = models.DateTimeField(auto_now_add=True)


# Asignación de tareas a proyectos
class TaskAssignment(models.Model):
    """ Model Task Form Template: save json form """
    task = models.ForeignKey(
        'Task',
        on_delete=models.CASCADE,
        null=True,
        related_name='assignment'
    )
    project = models.ForeignKey(
        'project.Project',
        on_delete=models.CASCADE,
        null=True,
        related_name='task_assignments'
    )

    class Meta:
        db_table = 'task_assignment'

    def __str__(self):
        try:
            return "%s (%s)" % (self.task.name, self.store.name)
        except(ObjectDoesNotExist, AttributeError):
            return ''

    def is_saved(self, date):
        try:
            r = TaskAssignmentResult.objects.filter(
                #task_assignment=self,
                date__day=date.day,
                date__month=date.month,
                date__year=date.year
            )[:1].get()
            return {'object': r, 'status': True}
        except TaskAssignmentResult.DoesNotExist:
            return {'object': None, 'status': False}
    
    def next_task_assignment(self):
        next_assignment = TaskAssignment.objects.filter(
            task__seguimiento__tarea_completada=0,
            project=self.project,
            task__parent=self.task,
        ).first()
        if (next_assignment):
            return next_assignment

    def is_auditee(self, date):
        try:
            r = TaskAssignmentResult.objects.filter(
                task_assignment=self,
                date__day=date.day,
                date__month=date.month,
                date__year=date.year
            )[:1].get()
            return {'object': r, 'status': True}
        except TaskAssignmentResult.DoesNotExist:
            return {'object': None, 'status': False}


class SeguimientoTareas(models.Model):
    tarea = models.CharField(max_length=50),
    task = models.ForeignKey(Task, related_name="seguimiento", on_delete=models.DO_NOTHING)
    tarea_completada = models.BooleanField()
    nombre_tienda = models.CharField(max_length=50)

    class Meta:
        db_table = 'seguimiento_tareas'
        managed = False

    def delete(self, *args, **kwargs):
        pass # deletion of a view row is never valid

class AvancesProyectoView(models.Model):
    proyecto = models.ForeignKey('project.Project', related_name="projects", on_delete=models.DO_NOTHING)
   #proyecto = models.TextField(blank=True, null=True)
    tienda = models.TextField(blank=True, null=True)
    store = models.ForeignKey('store.Store', related_name="stores", on_delete=models.DO_NOTHING)
    location = models.ForeignKey('store.Location', related_name="locations", on_delete=models.DO_NOTHING)
    ubicacion = models.TextField(blank=True, null=True)
    avance = models.TextField(blank=True, null=True)
    class Meta:
        db_table = 'avances_proyecto'
        managed = False

    def delete(self, *args, **kwargs):
        pass # deletion of a view row is never valid

class AsignacionesDeTareas(models.Model):
    task = models.ForeignKey('app.Task', related_name="asgTasks", on_delete=models.DO_NOTHING)
    assignment_to_project = models.ForeignKey('app.TaskAssignment', related_name="assignProjects", on_delete=models.DO_NOTHING)
    project = models.ForeignKey('project.Project', related_name="asgProjects", on_delete=models.DO_NOTHING)
    assignment_to_store = models.ForeignKey('app.TaskStore', related_name="assignStores", on_delete=models.DO_NOTHING)
    store = models.ForeignKey('store.Store', related_name="asgStores", on_delete=models.DO_NOTHING)

    class Meta:
        db_table = 'vw_project_and_store_task_assigments'
        managed = False

    def delete(self, *args, **kwargs):
        pass # deletion of a view row is never valid


class TaskAssignmentResult(models.Model):
    """ Model Task Form Result: copy of a template form with result in json """
    task = models.ForeignKey('app.Task', on_delete=models.CASCADE, related_name="task_responses")
    store = models.ForeignKey('store.Store', on_delete=models.CASCADE, related_name="store_task_results")
    project = models.ForeignKey('project.Project', null=True, blank=True, on_delete=models.CASCADE, related_name="project_task_responses")
    result = models.TextField(blank=True, null=True)
    date = models.DateField(blank=True, null=True, auto_now_add=True)
    audit_result = models.TextField(null=True)
    audit_date = models.DateTimeField(blank=True, null=True)
    audit_score = models.IntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    coords = models.PointField(null=True, blank=True, verbose_name="Coordenadas")

    def save(self, *args, **kwargs):
        self.date = datetime.now()
        super(TaskAssignmentResult, self).save(*args, **kwargs)

    def get_score(self):
        return self.audit_score

    def get_score_total(self):
        return self.audit_score

    class Meta:
        db_table = 'task_assignment_result'

    def __str__(self):
        return "%s (%s)" % (self.task_assignment, self.date)


class FormField (models.Model):
    """ Model Task Form Field  """
    type = models.CharField(max_length=30, choices=FORM_TYPE_CHOICES)
    label = models.CharField(max_length=30, blank=False, null=False)
    min_value = models.IntegerField(default=None, blank=True, null=True)
    max_value = models.IntegerField(default=None, blank=True, null=True)
    option = models.TextField(default=" ", blank=True, null=True)
    result = models.TextField(default=" ", blank=True, null=True)
    class_imput = models.CharField(
        max_length=30,
        choices=FORM_CLASS_CHOICES,
        default=" "
    )
    task = models.ForeignKey(
        'Task',
        on_delete=models.CASCADE,
        null=True,
        related_name='form'
    )

    class Meta:
        db_table = 'form_field'

    def __str__(self):
        return "%s%s" % (self.type, self.id)

    def get_html(self):
        if self.type in ('checkbox', 'text', ):
            return (
                '<input id="%s%s" type="%s" '
                'name="%s%s" class="%s" required >' %
                (
                    self.type,
                    self.id,
                    self.type,
                    self.type,
                    self.id,
                    self.class_imput
                )
            )
        if self.type in ('number', ):
            return (
                '<input id="%s%s" type="%s" name="%s%s" class="%s" '
                'min="%s" max="%s" data-plugin="touchSpin" required >' %
                (
                    self.type,
                    self.id,
                    self.type,
                    self.type,
                    self.id,
                    self.class_imput,
                    self.min_value,
                    self.max_value,
                )
            )
        if self.type in ('radio', ):
            option = '<fieldset id="group%s">' % (self.id)
            i = 1
            for o in self.option.split(','):
                option += (
                    '<input value="%s%s" type="%s" name="group%s"'
                    '  class="%s" required >%s<br>' %
                    (self.type, i, self.type, self.id, self.class_imput, o)
                )
                i += 1
            option += '</fieldset>'

            return option

class TaskEvidence(models.Model):
    assignment_result = models.ForeignKey(
        to='TaskAssignmentResult',
        on_delete=models.CASCADE,
        related_name="evidences"
    )
    evidence = models.ImageField(upload_to="static/files/task_evidences")


class OTWSystemLog(models.Model):
    who = models.ForeignKey('user_profile.user', on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    action = models.CharField(max_length=200)
    details = models.TextField()
    readable_details = models.TextField()
